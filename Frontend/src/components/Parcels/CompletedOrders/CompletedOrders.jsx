import React, { useEffect, useState } from 'react';
import {
  FormControl,
  Checkbox,
  Paper,
  TableSortLabel,
  TableRow,
  TablePagination,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  Table,
} from '@material-ui/core';
import { useContext } from 'react';
import { AuthContext } from '../../../providers/AuthProvider';
import { getParcelsByUserId } from '../../../requests/ParcelRequests';
import useStyles from './myOrdersStyles';
import { shipmentStatus } from '../../../common/shipment-status';
import Error from '../../Base/Error/Error';

const CompletedOrders = () => {
  const [myParcels, setMyParcels] = useState([]);
  const [error, setError] = useState(null);
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const { user, darkTheme } = useContext(AuthContext);


  useEffect(() => {
    getParcelsByUserId(user.id)
      .then((result) => {
        if (result.data) {
          const completed = result.data.completedParcels;
          if (completed.length > 0) {
            setMyParcels(completed);
          } else {
            setError('No parcels on the way!');
          }
        } else {
          setError(result.error);
        }
      })
  }, [user.id]);

  const classes = useStyles();

  const rows = myParcels.map((parcel) => ({
    parcelId: parcel.id,
    status: parcel.status,
    weight: parcel.weight,
    category: parcel.category,
    location: parcel.parcel_location,
    shipmentId: parcel.shipment_id,
  }));

  const headCells = [
    { id: 'parcelId', numeric: false, disablePadding: true, label: 'ID' },
    { id: 'shipmentId', numeric: false, disablePadding: true, label: 'Shipment ID' },
    { id: 'location', numeric: false, disablePadding: true, label: 'Warehouse Address' },
    { id: 'status', numeric: true, disablePadding: false, label: 'Status' },
    { id: 'weight', numeric: true, disablePadding: false, label: 'Weight' },
    { id: 'category', numeric: true, disablePadding: false, label: 'Category' },
  ];

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const EnhancedTableHead = (props) => {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ 'aria-label': 'select all desserts' }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align="center"
              padding='normal'
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }

  const EnhancedTable = () => {
    const handleRequestSort = (event, property) => {
      const isAsc = orderBy === property && order === 'asc';
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
      if (event.target.checked) {
        const newSelecteds = rows.map((n) => n.parcelId);
        setSelected(newSelecteds);
        return;
      }
      setSelected([]);
    };

    const handleClick = (event, parcelId) => {
      const selectedIndex = selected.indexOf(parcelId);
      let newSelected = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, parcelId);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }
      setSelected(newSelected);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const isSelected = (parcelId) => selected.indexOf(parcelId) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
      <div className={classes.root}>
        <Paper className={darkTheme ? classes.paperDark : classes.paper}>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size='medium'
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}

              />
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(row.parcelId);
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        onClick={(event) => handleClick(event, row.parcelId)}
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={row.parcelId}
                        selected={isItemSelected}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                        </TableCell>
                        <TableCell component="th" id={labelId} scope="row" padding="none" align="center">
                          {row.parcelId}
                        </TableCell>
                        <TableCell align="center">{row.shipmentId} </TableCell>
                        <TableCell align="center">{row.location} </TableCell>
                        <TableCell align="center">{shipmentStatus[row.status]}</TableCell>
                        <TableCell align="center">{row.weight}</TableCell>
                        <TableCell align="center">{row.category}</TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </div>
    );
  }
  return (
    <div className={classes.parcelsContainer}>
      <h2 className={classes.title}>COMPLETED ORDERS</h2>
      {error ? <Error message={error} /> :
        <FormControl >
          {EnhancedTable()}
        </FormControl>}
    </div>
  );
};

export default CompletedOrders;