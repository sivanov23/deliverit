import React, { useEffect, useState, useRef, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TableSortLabel } from '@material-ui/core';
import { Button, Menu, MenuItem, IconButton, ListItemIcon, ListItemText, Paper, Checkbox, FormControl } from '@material-ui/core';
import { Icon } from '@iconify/react';
import editFill from '@iconify/icons-eva/edit-fill';
import moreVerticalFill from '@iconify/icons-eva/more-vertical-fill';
import trash2Outline from '@iconify/icons-eva/trash-2-outline';
import { AuthContext } from '../../../providers/AuthProvider';
import { deleteParcel, getAllParcels } from '../../../requests/ParcelRequests';
import { shipmentStatus } from '../../../common/shipment-status';
import { parcelCategories } from '../../../common/categories';
import Error from '../../Base/Error/Error'
import useStyles from './AllParcelsStyles';

const AllParcels = () => {
  const [allParcels, setAllParcels] = useState([]);
  const [error, setError] = useState(null);
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const { darkTheme } = useContext(AuthContext);
  const history = useHistory();

  useEffect(() => {
    getAllParcels()
      .then((result) => {
        if (result.data) {
          setAllParcels(result.data);
        } else {
          setAllParcels([]);
        }
      })
  }, []);

  const createParcel = () => {
    history.push('/parcels/new')
  }

  const classes = useStyles();

  const handleDeleteParcel = (parcelIds) => {
    if (parcelIds.length) {
      parcelIds.forEach(pId => {
        deleteParcel(pId)
          .then(result => {
            if (result.error) {
              setError(result.error);
            } else {
              if (result.data.allParcels === null) {
                setAllParcels([]);
                setError("Parcel not found!");
              }
              setAllParcels(result.data.allParcels);
            }
          });
      })
    };

   setTimeout(() => getAllParcels()
      .then((result) => {
        if (result.data) {
          setAllParcels(result.data);
        } else {
          setAllParcels([]);
        }
      }), 200);

      setSelected()
  };

  const handleEditParcel = (parcelIds) => {
    if (parcelIds.length !== 1) {
      setError("You can only select one parcel to edit")
    } else {
      history.push(`/parcels/update/${parcelIds[0]}`);
    }
  }

  const rows = allParcels.map((parcel) =>
    ({ parcelId: parcel.id, fullName: parcel.full_name, status: parcel.status, weight: parcel.weight, category: parcel.category }));

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const headCells = [
    { id: 'parcelId', numeric: false, disablePadding: true, label: 'ID' },
    { id: 'fullName', numeric: false, disablePadding: false, label: 'Full Name' },
    { id: 'status', numeric: true, disablePadding: false, label: 'Status' },
    { id: 'weight', numeric: true, disablePadding: false, label: 'Weight' },
    { id: 'category', numeric: true, disablePadding: false, label: 'Category' },
    { id: '' }
  ];



  const EnhancedTableHead = (props) => {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ 'aria-label': 'select all parcels' }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={headCell.numeric ? 'right' : 'left'}
              padding={headCell.disablePadding ? 'none' : 'normal'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }


  const EnhancedTable = () => {
    const handleRequestSort = (event, property) => {

      const isAsc = orderBy === property && order === 'asc';
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
      if (event.target.checked) {
        const newSelecteds = rows.map((n) => n.parcelId);
        setSelected(newSelecteds);
        setError(null);
        return;
      }
      setSelected([]);
      setError(null);
    };

    const handleClick = (event, parcelId) => {
      const selectedIndex = selected.indexOf(parcelId);
      let newSelected = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, parcelId);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }
      setSelected(newSelected);
      setError(null);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const isSelected = (parcelId) => selected.indexOf(parcelId) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
      <div className={classes.root}>
        <Paper className={darkTheme ? classes.paperDark : classes.paper}>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={'medium'}
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(row.parcelId);
                    const labelId = `enhanced-table-checkbox-${index}`;
                    return (
                      <TableRow
                        hover
                        onChange={(event) => handleClick(event, row.parcelId)}
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={row.parcelId}
                        selected={isItemSelected}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                        </TableCell>
                        <TableCell component="th" id={labelId} scope="row" padding="none" align="left">
                          {row.parcelId}
                        </TableCell>
                        <TableCell align="left">{row.fullName}</TableCell>
                        <TableCell align="right">{shipmentStatus[row.status]}</TableCell>
                        <TableCell align="right">{row.weight}</TableCell>
                        <TableCell align="right">{parcelCategories[row.category]}</TableCell>
                        <TableCell align="right">
                          <ParcelMoreMenu />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </div>
    );
  };

  const ParcelMoreMenu = () => {
    const ref = useRef(null);
    const [isOpen, setIsOpen] = useState(false);

    return (
      <>
        <IconButton ref={ref} onClick={() => setIsOpen(true)}>
          <Icon icon={moreVerticalFill} width={20} height={20} />
        </IconButton>
        <Menu
          open={isOpen}
          anchorEl={ref.current}
          onClose={() => setIsOpen(false)}
          PaperProps={{
            sx: { width: 200, maxWidth: '100%' }
          }}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        >
          <MenuItem sx={{ color: 'text.secondary' }} onClick={() => handleDeleteParcel(selected)}>
            <ListItemIcon>
              <Icon icon={trash2Outline} width={24} height={24} />
            </ListItemIcon>
            <ListItemText primary="Delete" primaryTypographyProps={{ variant: 'body2' }} />
          </MenuItem>
          <MenuItem sx={{ color: 'text.secondary' }} onClick={() => handleEditParcel(selected)}>
            <ListItemIcon>
              <Icon icon={editFill} width={24} height={24} />
            </ListItemIcon>
            <ListItemText primary="Edit" primaryTypographyProps={{ variant: 'body2' }} />
          </MenuItem>
        </Menu>
      </>
    );
  }

  return (
    <div className={classes.parcelsContainer}>
      <h2 className={classes.title}>ALL PARCELS</h2>
      <br />
      {error ?
        <div className={classes.error}>
          <Error message={error} />
        </div> : null
      }
      <FormControl >
        {EnhancedTable()}
      </FormControl>
      <Button className={classes.createButton} variant="contained" color="primary" onClick={createParcel}>Create parcel</Button>
      <br />
    </div>
  );
};

export default AllParcels;