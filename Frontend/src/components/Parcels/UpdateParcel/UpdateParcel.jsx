/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useContext, useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import {
  Button, 
  TextField, 
  Box, 
  Typography, 
  Container, 
  FormControl,
  InputLabel,
  MenuItem,
  Select
} from '@material-ui/core';
import WorkIcon from '@material-ui/icons/Work';
import { AuthContext } from '../../../providers/AuthProvider';
import { getSingleParcel, putParcel } from '../../../requests/ParcelRequests';
import { getAllShipmentsByStatus } from '../../../requests/ShipmentRequests';
import { useStyles } from './UpdateParcelStyles';

const parcelUpdate = () => {
  const { id } = useParams();
  const history = useHistory();
  const { darkTheme } = useContext(AuthContext);

  const [openShipment, setOpenShipment] = useState(false);
  const [delivery, setDelivery] = React.useState('');
  const [open, setOpen] = React.useState(false);
  const [shipments, setShipments] = useState([]);
  const [parcelDetails, setParcelDetails] = useState({
    weight: null,
    category: null,
    shipmentId: '',
    deliverToAddress: '',
    userId: ''
  });

  useEffect(() => {
    getAllShipmentsByStatus()
      .then(result => {
        if (result.data) {
          setShipments(result.data);
        }
      })
  }, []);

  useEffect(() => {
    getSingleParcel(id)
    .then(result => {
      if(result.data){
        setParcelDetails({...result.data , userId: result.data.user_id});
      }
    })
  },[id]);

  const updateParcel = () => {
    putParcel(parcelDetails, id);
    setTimeout(() => history.push('/parcels'), 1000);
  };

  const handleChange = (event) => {
    setDelivery(event.target.value);
    setParcelDetails({ ...parcelDetails, deliverToAddress: event.target.value });
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleCloseShipment = () => {
    setOpenShipment(false);
  };

  const handleOpenShipment = () => {
    setOpenShipment(true);
  };

  const classes = useStyles();

  const update = () => {
    return (
      <Container className={darkTheme ? classes.containerDark : classes.container} component="main" maxWidth="xs">
        <div className={classes.paper}>
          <div className={classes.iconBackground}>
            <WorkIcon className={classes.avatar} />
          </div>
          <Typography component="h1" variant="h5">  
            UPDATE PARCEL
          </Typography>
          <div className={classes.neatForm}>
            <div className={classes.formElement}>
              <TextField
                className={classes.formControl}
                autoComplete="weight"
                name="weight"
                variant="outlined"
                required
                fullWidth
                id="weight"
                label="Weight"
                onChange={(e) => setParcelDetails({ ...parcelDetails, weight: e.target.value })} value={parcelDetails.weight} type="text" placeholder="weight"
                autoFocus
              />
            </div>
            <div className={classes.formElement}>
              <TextField
                className={classes.formControl}
                variant="outlined"
                required
                fullWidth
                id="category"
                label="Category"
                name="category"
                onChange={(e) => setParcelDetails({ ...parcelDetails, category: e.target.value })} value={parcelDetails.category} type="text" placeholder="category"
                autoComplete="category"
              />
            </div>
            <div className={classes.formElement}>
              <TextField
                className={classes.formControl}
                variant="outlined"
                required
                fullWidth
                id="user"
                label="Customer"
                name="user"
                value={parcelDetails.user_id}
                onChange={(e) => setParcelDetails({ ...parcelDetails, userId: e.target.value })}  type="text" placeholder="user"
                autoComplete="user"
              />
            </div>
            <div className={classes.formElement}>
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-controlled-open-select-label">Delivery</InputLabel>
                <Select
                  labelId="demo-controlled-open-select-label"
                  id="demo-controlled-open-select"
                  open={open}
                  onClose={handleClose}
                  onOpen={handleOpen}
                  value={delivery}
                  onChange={handleChange}
                >
                  <MenuItem value={0}>Warehouse</MenuItem>
                  <MenuItem value={1}>Home Address</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div className={classes.formElement}>
                <FormControl className={classes.formControl}>
                  <InputLabel id="shipments">Shipment ID</InputLabel>
                  <Select
                    labelId="shipments"
                    id="shipments"
                    open={openShipment}
                    onClose={handleCloseShipment}
                    onOpen={handleOpenShipment}
                    onChange={(e) => setParcelDetails({ ...parcelDetails, shipmentId: e.target.value })} value={parcelDetails.shipmentId} type="text" placeholder="shipments"
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {shipments.map(shipments => { return <MenuItem  key={shipments.id} value={shipments.id}>{shipments.id}</MenuItem> })}
                  </Select>
                </FormControl>
             </div>
             <Button
             fullWidth
             variant="contained"
             color="primary"
             className={classes.submit}
             onClick={() => updateParcel(parcelDetails)}
             >
             Update Parcel
            </Button>           
          </div>
        </div>
        <Box mt={5}>
        </Box>
      </Container>
    );
  };

  return (
    <div className={classes.shipmentsContainer}>
      <h2 className={classes.title}>UPDATE PARCEL</h2>
      <br />
      <FormControl >
        {update()}
      </FormControl>
      <br />
    </div>
  );
};


export default parcelUpdate;


