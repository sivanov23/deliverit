import React, { useState, useEffect,useContext } from 'react';
import { 
  Button,
  TextField,
  Box,
  Typography,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select
} from '@material-ui/core';
import WorkIcon from '@material-ui/icons/Work';
import { getAllShipmentsByStatus } from '../../../requests/ShipmentRequests.js';
import { AuthContext } from '../../../providers/AuthProvider.jsx';
import { postParcel } from '../../../requests/ParcelRequests.js';
import { useStyles } from './createParcelStyles.js';
import { useHistory } from 'react-router-dom';

const CreateParcel = () => {
  const { darkTheme } = useContext(AuthContext);
  const history = useHistory();
  const [openShipment, setOpenShipment] = useState(false);
  const [shipments, setShipments] = useState([]);
  const [delivery, setDelivery] = React.useState('');
  const [open, setOpen] = React.useState(false);
  const [parcelToRegister, setParcelToRegister] = useState({
    weight: "",
    category: "",
    userId: "",
    deliverToAddress: "",
    shipmentId: "",
  });


const newParcel = ({ weight, category, userId, deliverToAddress, shipmentId }) => {
  postParcel({ weight, category, userId, deliverToAddress, shipmentId }); 
  history.push('/parcels');
};

useEffect(() => {
  getAllShipmentsByStatus()
  .then(result => {
    if(result.data){
      console.log(result.data)
      setShipments(result.data);
    }
  })
},[]);


const handleChange = (event) => {
  setDelivery(event.target.value);
  setParcelToRegister({...parcelToRegister, deliverToAddress: event.target.value});
};

const handleClose = () => {
  setOpen(false);
};

const handleOpen = () => {
  setOpen(true);
};

const handleCloseShipment = () => {
  setOpenShipment(false);
};

const handleOpenShipment = () => {
  setOpenShipment(true);
};

const classes = useStyles();
 
const ValidationTextFields = () => {
  
  return  (
    <Container className={darkTheme? classes.containerDark : classes.container} component="main" maxWidth="xs">
      <div className={classes.paper}>
        <div className={classes.iconBackground}>
          <WorkIcon className={classes.avatar} />
        </div>
        <Typography component="h1" variant="h5">  
          CREATE PARCEL
        </Typography>
        <div className={classes.neatForm}>
         <div className={classes.formElement}>
                <TextField
              className={classes.formControl}
                  autoComplete="weight"
                  name="weight"
                  variant="outlined"
                  required
                  fullWidth
                  id="weight"
                  label="Weight"
                  onChange={(e) => setParcelToRegister({ ...parcelToRegister, weight: e.target.value })} value={setParcelToRegister.weight} type="text" placeholder="weight"
                  autoFocus
                />  
            </div>
            <div className={classes.formElement}>
                <TextField
              className={classes.formControl}
                  autoComplete="category"
                  name="category"
                  variant="outlined"
                  required
                  fullWidth
                  onChange={(e) => setParcelToRegister({ ...parcelToRegister, category: e.target.value })} value={setParcelToRegister.category} type="text" placeholder="category"
                  id="category"
                  label="Category"

                  autoFocus
                />  
                  </div>
            <div className={classes.formElement}>
                <TextField
              className={classes.formControl}
                  autoComplete="userId"
                  name="userId"
                  variant="outlined"
                  required
                  fullWidth
                  id="userId"
                  label="Customer"
                  onChange={(e) => setParcelToRegister({ ...parcelToRegister, userId: e.target.value })} value={setParcelToRegister.userId} type="text" placeholder="userId"
                  autoFocus
                /> 
                   </div>
            <div className={classes.formElement}>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-controlled-open-select-label">Deliver To:</InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={delivery}
          onChange={handleChange}
        >
          <MenuItem value={0}>Warehouse</MenuItem>
          <MenuItem value={1}>Home Address</MenuItem>
        </Select>
      </FormControl>
                 </div>
    <div className={classes.formElement}>
      <FormControl className={classes.formControl}>
        <InputLabel id="shipments">Shipment ID</InputLabel>
        <Select
          labelId="shipments"
          id="shipments"
          open={openShipment}
          onClose={handleCloseShipment}
          onOpen={handleOpenShipment}
          onChange={(e) => setParcelToRegister({ ...parcelToRegister, shipmentId: e.target.value })} value={parcelToRegister.shipmentId} type="text" placeholder="shipments"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
         
          {shipments.map(shipments => { return <MenuItem  key={shipments.id} value={shipments.id}>{shipments.id}</MenuItem> })}
          
        </Select>
      </FormControl>
    </div>
          <Button
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          onClick={() => newParcel(parcelToRegister)}
        >
         CREATE PARCEL
        </Button>
        </div>
      </div>
      <Box mt={5}>
      </Box>
    </Container>
    
  );
}
return (
  <div className={classes.shipmentsContainer}>
    <h2 className={classes.title}>UPDATE PARCEL</h2>
    <br />
    <FormControl >
      {ValidationTextFields()}
    </FormControl>

    <br />
  </div>
);
}
export default CreateParcel;


