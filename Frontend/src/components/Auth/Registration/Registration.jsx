import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Avatar,
  Button,
  CssBaseline,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select
} from '@material-ui/core';
import { getAllCitiesByCountry, getAllCountries } from '../../../requests/LocationRequests';
import { createUser } from '../../../requests/UserRequests';
import { useStyles } from './RegistrationStyles.js';
import { AuthContext } from '../../../providers/AuthProvider';

const Register = () => {
  const history = useHistory();
  const {darkTheme} = useContext(AuthContext);
  const [openCountry, setOpenCountry] = useState(false);
  const [openCity, setOpenCity] = useState(false);
  const [countries, setCountries] = useState([]);
  const [cities, setCities] = useState([]);
  const [userToRegister, setUserToRegister] = useState({
    email: "",
    fullName: "",
    countryId: "",
    cityId: "",
    street: "",
    password: "",
  });

  useEffect(() => {
    getAllCountries()
      .then((result) => setCountries(result.data));
  }, []);

  useEffect(() => {
    getAllCitiesByCountry(userToRegister.countryId)
      .then((result) => setCities(result.data));
  }, [userToRegister.countryId]);

  const handleCloseCountry = () => {
    setOpenCountry(false);
  };

  const handleOpenCountry = () => {
    setOpenCountry(true);
  };

  const handleCloseCity = () => {
    setOpenCity(false);
  };

  const handleOpenCity = () => {
    setOpenCity(true);
  };

  const register = ({ email, fullName, countryId, password, cityId, street }) => {
    createUser({ email, fullName, countryId, password, cityId, street });
    history.push('/login');
  };

  const SignUp = () => {
    const classes = useStyles();
    return (
      <Container className={darkTheme ? classes.containerDark : classes.container} component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar} />
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} >
                <TextField
                  autoComplete="fullname"
                  name="fullname"
                  variant="outlined"
                  required
                  fullWidth
                  id="fullName"
                  label="Full Name"
                  onChange={(e) => setUserToRegister({ ...userToRegister, fullName: e.target.value })} value={userToRegister.fullName} type="text" placeholder="fullName"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  onChange={(e) => setUserToRegister({ ...userToRegister, email: e.target.value })} value={userToRegister.email} type="text" placeholder="email"
                />
              </Grid>

              <Grid item xs={12}>
                <div>
                  <FormControl className={classes.formControl}>
                    <InputLabel id="Countries">Country</InputLabel>
                    <Select
                      labelId="Countries"
                      id="Countries"
                      open={openCountry}
                      onClose={handleCloseCountry}
                      onOpen={handleOpenCountry}
                      onChange={(e) => setUserToRegister({ ...userToRegister, countryId: e.target.value })} value={userToRegister.countryId} type="text" placeholder="country"
                    >
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      {countries.length
                        ? countries.map(country => { return <MenuItem key={country.id} value={country.id}>{country.name}</MenuItem> })
                        : null}
                    </Select>

                  </FormControl>
                </div>
              </Grid>

              <Grid item xs={12}>
                <div>
                  <FormControl className={classes.formControl}>
                    <InputLabel id="city">City</InputLabel>
                    <Select
                      labelId="Cities"
                      id="Cities"
                      open={openCity}
                      onClose={handleCloseCity}
                      onOpen={handleOpenCity}
                      onChange={(e) => setUserToRegister({ ...userToRegister, cityId: e.target.value })} value={userToRegister.cityId} type="text" placeholder="city"
                    >
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>

                      {cities.length
                        ? cities.map(city => { return <MenuItem key={city.id} value={city.id}>{city.city_name}</MenuItem> })
                        : null}
                    </Select>
                  </FormControl>
                </div>
              </Grid>

              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={(e) => setUserToRegister({ ...userToRegister, street: e.target.value })} value={userToRegister.street} type="text" placeholder="street"
                  id="street"
                  label="Address"
                  name="street"
                  autoComplete="Street"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={(e) => setUserToRegister({ ...userToRegister, password: e.target.value })} value={userToRegister.password} type="password" placeholder="password"
                  name="password"
                  label="Password"
                  id="password"
                  autoComplete="current-password"
                />
              </Grid>
            </Grid>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => register(userToRegister)}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link variant="body2" onClick={() => history.push('/login')}>
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}>
        </Box>
      </Container>
    );
  };

  return (
    <div className="register-container">
      <br />
      <FormControl >
        {SignUp()}
      </FormControl>   
      <br/>
    </div>
  );
};


export default Register;


