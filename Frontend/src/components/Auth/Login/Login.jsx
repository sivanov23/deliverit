import { useContext, useState, React } from 'react';
import { useHistory } from 'react-router-dom';
import { 
  Avatar, 
  Button, 
  CssBaseline, 
  TextField, 
  FormControlLabel, 
  Checkbox, 
  Box, 
  Typography, 
  Container
} from '@material-ui/core';
import { AuthContext } from '../../../providers/AuthProvider.jsx';
import getUserFromToken from '../../../utils/token.js';
import { login } from '../../../requests/UserRequests.js';
import { useStyles } from './LoginStyles.js';


const Login = () => {
  const classes = useStyles();
  const history = useHistory();
  const { darkTheme, setUser } = useContext(AuthContext);

  const [email, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const triggerLogin = (email, password) => {
    if (!email || !password) {
      return alert('Invalid credentials!');
    }
    login(email, password)
      .then(data => {
        localStorage.setItem('token', data.token);
        setUser(getUserFromToken(localStorage.getItem('token')));
      })
      .catch(e => console.error(e));
    history.push('/home');
  };

  return (
    <Container className={darkTheme ? classes.containerDark : classes.container} component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={e => setUsername(e.target.value)} value={email} type="text" placeholder="Email"

          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={e => setPassword(e.target.value)} value={password} placeholder="******"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={() => triggerLogin(email, password)}
          >
            Sign In
          </Button>
        </form>
      </div>
      <Box mt={8}>
      </Box>
    </Container>
  );
}

export default Login;
