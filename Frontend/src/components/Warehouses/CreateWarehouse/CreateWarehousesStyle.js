import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    margin: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    background: 'rgb(250,250,250)',
    border: '4px solid royalblue',
    borderRadius: '10px',
    width: '800px',
  },
  contentContainerDark: {
    margin: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    background: 'lightgray',
    border: '4px solid royalblue',
    borderRadius: '10px',
    width: '800px',
  },
  formControl: {
    width: '120px',
    margin: theme.spacing(2),
    padding: '10px',
    backgroundColor: 'rgb(250,250,250)',
  },
  formControlDark: {
    width: '120px',
    margin: theme.spacing(2),
    padding: '10px',
    backgroundColor: 'lightgray',
  },
  searchInfo: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    margin: theme.spacing(4, 2, 2, 2),
    backgroundColor: 'rgb(250,250,250)',
    borderRadius: '10px',
    border: '2px dotted royalblue',
    width: '662px',
  },
  searchInfoDark: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    margin: theme.spacing(4, 2, 2, 2),
    backgroundColor: 'lightgray',
    borderRadius: '10px',
    border: '2px dotted royalblue',
    width: '700px',
  },
  textbox: {
    backgroundColor: 'rgb(250,250,250)',
    borderRadius: '10px',
    margin: theme.spacing(2),
    padding: '10px'
  },
  textboxDark: {
    backgroundColor: 'lightgray',
    borderRadius: '10px',
    margin: theme.spacing(2),
    padding: '10px'
  },
  searchButton: {
    margin: theme.spacing(2),
    height: theme.spacing(7)
  },
  createButton: {
    margin: theme.spacing(2, 2, 4, 2),
    height: theme.spacing(7)
  },
  error: {
    margin: theme.spacing(2, 2, 0, 2),
  },
  map: {
    display: 'flex',
    width: '500px',
    height: '500px',
    justifyContent: 'center',
    alignItems: 'center',
    border: '2px dotted royalblue',
    borderRadius: '20px',
    margin: '16px 0px',
  }

}));

export default useStyles;