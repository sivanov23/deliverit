import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    margin: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    background: 'rgb(250,250,250)',
    border: '4px solid royalblue',
    borderRadius: '10px',
    width: '800px',
  },
  contentContainerDark: {
    margin: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    background: 'lightgray',
    border: '4px solid royalblue',
    borderRadius: '10px',
    width: '800px',
  },
  error: {
    margin: theme.spacing(2, 2, 0, 2),
  },
  map: {
    display: 'flex',
    width: '700px',
    height: '700px',
    justifyContent: 'center',
    alignItems: 'center',
    border: '2px dotted royalblue',
    borderRadius: '20px',
    margin: '16px 0px',
  },
  formControl: {
    width: '420px',
    margin: theme.spacing(4,0,2,0),
  },
  popup:{
    display: 'flex',
    flexDirection: 'column',
    alignItems:'center',
    justifyContent: 'center',
    width: "auto",
  },
  popupIcons:{
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  delete:{
    borderRadius: '10px',
    '&:hover':{
      backgroundColor: 'rgba(221, 22, 22, 0.5)',
    },
    '&:active': {
      backgroundColor: 'rgba(221, 22, 22, 0.8)',
    },
  },
   edit: {
    borderRadius: '10px',
    '&:hover': {
      backgroundColor: 'rgba(65, 105, 225, 0.5)',
    },
    '&:active': {
      backgroundColor: 'rgba(65, 105, 225, 0.8)',
    },
  }, 
  createButton: {
    margin: theme.spacing(2,2,4,2),
    height: theme.spacing(7)
  },
}));

export default useStyles;