
import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import L from 'leaflet';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import { Button, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { AuthContext } from '../../../providers/AuthProvider';
import { deleteWarehouse, getWarehouses } from '../../../requests/WarehouseRequests';
import useStyles from './AllWarehousesStyle';
import marker from '../../../common/images/marker.png';
import Error from '../../Base/Error/Error';



const AllWarehouses = () => {

  const [warehouses, setWarehouses] = useState([]);
  const [error, setError] = useState(null);
  const [coordinates, setCoordinates] = useState([42.65000, 23.37932]);
  const [latlng, setLatlng] = useState(null);
  const [warehouseDetails, setWarehouseDetails] = useState({
    id: null,
    country: null,
    city: null,
    street: null,
    latitude: null,
    longitude: null
  });

  useEffect(() => {
    getWarehouses()
      .then(result => {
        if (result.data) {
          setWarehouses(result.data);
        } else {
          throw new Error(result.error);
        }
      }).catch(e => setError(e.message));
  }, [])

  const history = useHistory();

  const { user, darkTheme } = useContext(AuthContext);

  const classes = useStyles();

  const icon = new L.Icon({
    iconUrl: marker,
    iconSize: [25, 34],
    iconAnchor: [12.5, 34],
  });

  const takeCoordinates = (e) => {
    const warehousedetails = e.target.value;
    setWarehouseDetails(warehousedetails);
    setCoordinates([warehousedetails.latitude, warehousedetails.longitude]);
    setLatlng([warehousedetails.latitude, warehousedetails.longitude]);
  };

  const goToEditView = () => {
    const address = `/warehouses/${warehouseDetails.id}`;
    history.push(address);
  };

  const delWarehouse = () => {
    deleteWarehouse(warehouseDetails.id);
    const warehousesAfterDeletion = warehouses;
    const index = warehousesAfterDeletion.findIndex(w => (w.id === warehouseDetails.id));
    if(index > 0){
      warehousesAfterDeletion.splice(index, 1);
    }
    setWarehouses(warehousesAfterDeletion);
    setLatlng(null);
  };

  const createWarehouse = () => {
    history.push('/warehouses/new');
  }

  return (
    <div className={darkTheme ? classes.contentContainerDark : classes.contentContainer}>
      {!error ? <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel htmlFor="warehouses">Warehouse</InputLabel>
        <Select
          label="Warehouses"
          id="warehouses"
          onChange={takeCoordinates} 
          value={warehouseDetails} 
          type="text" 
          placeholder="Warehouse"
        >
          {warehouses.length
            ? warehouses.map(warehouse => { return <MenuItem key={warehouse.id} value={warehouse}>{`${warehouse.street}, ${warehouse.city}, ${warehouse.country}`}</MenuItem> })
            : null}
        </Select>
      </FormControl>
      : <Error message = {error}> </Error>}
      <Map className={classes.map} center={coordinates} flyTo={true} zoom={4} maxZoom={19}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
       {latlng ? <Marker position={latlng} icon={icon}>
          <Popup>
            <div className={classes.popup}>
              <p className={classes.warehouseDescription}>{warehouseDetails ? `${warehouseDetails.street}, ${warehouseDetails.city}, ${warehouseDetails.country}` : null}</p>
             {user 
             ? <div>
                <EditIcon className={classes.edit} fontSize="large" onClick={goToEditView} />
                <DeleteIcon className={classes.delete} fontSize="large" onClick={delWarehouse}/>
              </div> 
             : null}
            </div>
          </Popup>
        </Marker> : null}
      </Map>
     { user ? <Button className={classes.createButton} variant="contained" color="primary" onClick={createWarehouse}>Create warehouse</Button> : null}
    </div>
  )
};

export default AllWarehouses;