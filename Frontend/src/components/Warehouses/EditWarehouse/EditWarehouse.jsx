import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import L from 'leaflet';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import { InputLabel, MenuItem, TextField, FormControl, Select, Button } from '@material-ui/core';
import { AuthContext } from '../../../providers/AuthProvider';
import { getSingleWarehouse, putWarehouse } from '../../../requests/WarehouseRequests';
import Error from '../../../components/Base/Error/Error';
import marker from '../../../common/images/marker.png';
import useStyles from './EditWarehouseStyles';



const EditWarehouse = () => {

  const {id} = useParams();
  const history = useHistory();
  const { darkTheme } = useContext(AuthContext);

  const [coords, setCoords] = useState([42.65000, 23.37932]);
  const [content, setContent] = useState('');
  const [results, setResults] = useState(null);
  const [error, setError] = useState(false);
  const [stringifiedCoords, setStringifiedCoords] = useState('');
  const [warehouseDetails, setWarehouseDetails] = useState({
    country: null,
    city: null,
    street: null,
    latitude: null,
    longitude: null
  });

  useEffect(() => {
    getSingleWarehouse(id)
    .then(result => {
      if(result.data){
        setWarehouseDetails({...result.data});
        setContent(`${result.data.street},${result.data.city},${result.data.country}`);
        setCoords([result.data.latitude, result.data.longitude]);
      }
    })
  },[id]);

  const classes = useStyles();

  const selectAddress = (event) => {
    setStringifiedCoords(event.target.value);
    const stringCoords = event.target.value.split(/,/);
    console.log(stringCoords);
    setCoords(stringCoords);
    setWarehouseDetails({ ...warehouseDetails, latitude: +stringCoords[0], longitude: +stringCoords[1] });
  };

  const changeInput = (e) => {
    const data = e.target.value;
    setContent(data);
  }

  const icon = new L.Icon({
    iconUrl: marker,
    iconSize: [25, 34],
    iconAnchor: [12.5, 34],
  });

  const submitSearch = () => {
    if (content) {
      const adrs = content.trim().split(',');
      if (adrs.length === 3) {
        setError(false);
        setWarehouseDetails({ ...warehouseDetails, country: adrs[2], city: adrs[1], street: adrs[0] });
        fetch(`https://api.opencagedata.com/geocode/v1/json?q=${adrs.join(',')}&key=7d4cdc0d6a0a4ce7a54985797e8521f1&limit=10`)
          .then(res => res.json())
          .then(result => {
            if (result.results.length) {
              setResults(result.results);
            }
          });
      } else {
        setError(true);
      }
    }
  };

  const updateWarehouse = () => {
    putWarehouse(warehouseDetails, id);
    console.log(warehouseDetails);
    setTimeout(() => (history.push('/warehouses')), 1000);
  }

  const setMarkerCoorinates = (e) => {
    setCoords(e.latlng);
    console.log(`${e.latlng.lat}`);
    setWarehouseDetails({ ...warehouseDetails, latitude: e.latlng.lat, longitude: e.latlng.lng });
  }

  return (
    <div className={darkTheme ? classes.contentContainerDark : classes.contentContainer}>
      {error ? <Error className={classes.error} message='Address needs to be in the format "STREET,CITY,COUNTRY"' /> : null}
      <div className={darkTheme ? classes.searchInfoDark : classes.searchInfo}>
        <form className={darkTheme ? classes.textboxDark : classes.textbox} noValidate autoComplete="оff">
          <TextField id="standard-basic" label="Search location" placeholder="Street,City,Country" value={content} onChange={changeInput} variant='outlined' error={error} autoFocus />
        </form>
        <Button className={classes.searchButton} color='primary' variant='outlined' onClick={submitSearch}>Submit search</Button>
        {results && !error ?
          <FormControl variant="outlined" className={darkTheme ? classes.formControlDark : classes.formControl} >
            <InputLabel htmlFor="address-native-helper"> Select Address</InputLabel>
            <Select
              label="Select Address"
              inputProps={{
                name: 'select address',
                id: 'address-native-helper',
              }}
              value={stringifiedCoords}
              onChange={selectAddress}
            >
              {results ? results.map(r => {
                return (
                  <MenuItem key={r.geometry.lat} value={`${r.geometry.lat},${r.geometry.lng}`}>{r.formatted}</MenuItem>
                )
              }) : null}
            </Select>
          </FormControl>
          : null}
      </div>
      <Map className={classes.map} center={coords} zoom={18} maxZoom={19} onclick={setMarkerCoorinates}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        <Marker position={coords} icon={icon}>
          <Popup>
            {`This is where warehouse ${warehouseDetails.id} is located.`}
          </Popup>
        </Marker>
      </Map>
      <Button className={classes.createButton} color='primary' variant='contained' onClick={updateWarehouse} disabled={error}>Update warehouse</Button>
    </div>

  )
};

export default EditWarehouse;