import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import InvertColors from '@material-ui/icons/InvertColors';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../../../providers/AuthProvider';
import { Avatar } from '@material-ui/core';
import siteLogo from '../../../common/images/siteLogo.png';
import siteLogoDark from '../../../common/images/siteLogoDark.png';
import { userRole } from '../../../common/constants';



const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
  },
  siteLogoDark: {
    margin: '0px 10px',
    '&:hover': {
      backgroundColor: 'rgba(10,10,10,0.05)',
      borderRadius: '5px',
    },
    '&:active': {
      backgroundColor: 'rgba(10,10,10,0.4)',
      borderRadius: '5px',
    }
  },
  siteLogo: {
    margin: '0px 10px',
    '&:hover': {
      backgroundColor: 'rgba(255,255,255,0.05)',
      borderRadius: '5px',
    },
    '&:active': {
      backgroundColor: 'rgba(255,255,255,0.4)',
      borderRadius: '5px',
    }
  },
  text: {
    fontWeight: 'bold',
  },
  links: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: -theme.spacing(2),
  },
  avatar: {
    display: 'flex',
    margin: theme.spacing(1),
  },
  theme: {
    fontSize: '30px',
  },
  loginInformation: {
    display: 'flex',
    flexDirection: 'row',
    margin: '0px 10px',
  }
}));

const ButtonAppBar = () => {
  const classes = useStyles();
  const history = useHistory();
  const { user, setUser, darkTheme, setDarkTheme } = useContext(AuthContext);

  const redirectToLogin = () => {
    history.push('/login');
  };

  const redirectToRegister = () => {
    history.push('/register');
  };

  const logout = () => {
    localStorage.removeItem('token');
    setUser(null);
    history.push('/');
  };

  const redirectToHome = () => {
    history.push('/home');
  };

  const redirectToCustomers = () => {
    history.push('/customers');
  };

  const redirectToWarehousees = () => {
    history.push('/warehouses');
  };

  const redirectToShipments = () => {
    history.push('/shipments');
  };

  const redirectToMyCompletedOrders = () => {
    history.push('/parcels/my/completed');
  };

  const redirectToParcels = () => {
    history.push('/parcels');
  };

  const redirectToMyParcels = () => {
    history.push('/parcels/my');
  };

  const redirectToUsers = () => {
    history.push('/users');
  }


  const changeTheme = () => {
    setDarkTheme(!darkTheme);
  }

  const unregisteredUserOptions =
    <div>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToCustomers}>Our Customers</Button>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToWarehousees}>Our Warehouses</Button>
    </div>;

  const customerOptions =
    <div>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToMyParcels}>Incoming Parcels</Button>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToMyCompletedOrders}>Completed Orders</Button>
    </div>;

  const emploeyeeOptions =
    <div>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToParcels}>Parcels</Button>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToShipments}>Shipments</Button>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToWarehousees}>Warehouses</Button>
      <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToUsers}>Users</Button>
    </div>


  return (
    <AppBar className={classes.root}>
      <Toolbar>
        <div>
          <img src={darkTheme ? siteLogoDark : siteLogo} className={darkTheme ? classes.siteLogoDark : classes.siteLogo} height='50px' alt='site-logo' onClick={redirectToHome} />
        </div>
        <div className={classes.links}>
          {!!user ? (user.role === userRole.CUSTOMER ? customerOptions : emploeyeeOptions) : unregisteredUserOptions}
        </div>
        <div className={classes.loginInformation}>
          {!!user ?
            <>
              <Avatar className={classes.avatar} alt={user.fullName} src='/non-existent-image.jpg' />
              <IconButton edge='start' className={classes.menuButton} color={darkTheme ? 'default' : 'inherit'} aria-label='menu' onClick={changeTheme}>
                <InvertColors className={classes.theme} />
              </IconButton>
              <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={logout}>Logout</Button>
            </> :
            <>
              <IconButton edge='start' className={classes.menuButton} color={darkTheme ? 'default' : 'inherit'} aria-label='menu' onClick={changeTheme}>
                <InvertColors className={classes.theme} />
              </IconButton>
              <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToLogin}>Login</Button>
              <Button color={darkTheme ? 'default' : 'inherit'} className={classes.text} onClick={redirectToRegister}>Register</Button>
            </>
          }
        </div>
      </Toolbar>
    </AppBar>
  );
}

export default ButtonAppBar;