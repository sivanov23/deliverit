import React from "react";
import AndroidIcon from '@material-ui/icons/Android';
import './Error.css';

const Error = ({ message }) => {

  return (
    <div className="error-container">
      <div className="inner-container">
        <AndroidIcon fontSize="large" />
        <p className="error-text">{message}</p>
      </div>
    </div >
  );
};

export default Error;
