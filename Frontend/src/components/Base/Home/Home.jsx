import React from 'react';
import video from '../../../common/videos/dhl.mp4';

const Home = () => {
  return (
    // <iframe width="1600px" height="900px" src="https://www.youtube.com/embed/VFjVQ_Gtx6c?autoplay=1&mute=1&controls=0&loop=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <video width="100%" height="60%" autoPlay muted loop>
      <source src={video} type="video/mp4"/>
     </video>
  )
};

export default Home;