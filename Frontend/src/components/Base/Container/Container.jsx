import React, { useContext } from 'react';
import { AuthContext } from '../../../providers/AuthProvider';
import './Container.css';

const Container = ({ children }) => {

  const { darkTheme } = useContext(AuthContext);

  return (
    <div className={darkTheme ? "content-container-dark" : "content-container"}>
      {children}
    </div>
  )
}

export default Container;