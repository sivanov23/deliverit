/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, } from 'react';
import { useParams } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Error from '../../Base/Error/Error';
import { FormControl } from '@material-ui/core';
import { useStyles } from './updateUserStyles.js';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { useEffect } from 'react';



const BASE_URL = "http://localhost:5000";

const updateUser = () => {
  const {id} = useParams();
  const [alert, setAlert] = useState(null);
  const [openCountry, setOpenCountry] = useState(false);
  const [openCity, setOpenCity] = useState(false);
  const [countries, setCountries] = useState([]);
  const [cities, setCities] = useState([]);
  const [userToUpdate, setUserToUpdate] = useState({
    email: "",
    fullName: "",
    countryId: "",
    cityId: "",
    street: "",
    password: "",
  });

  useEffect(() => {
    fetch(`${BASE_URL}/countries/`, {
      method: "Get",
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => setCountries(result.data))
  }, []);

  useEffect(() => {
    fetch(`${BASE_URL}/countries/${userToUpdate.countryId}/cities`, {
      method: "Get",
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => setCities(result.data))
  }, [userToUpdate.countryId]);

  const handleCloseCountry = () => {
    setOpenCountry(false);
  };

  const handleOpenCountry = () => {
    setOpenCountry(true);
  };

  const handleCloseCity = () => {
    setOpenCity(false);
  };

  const handleOpenCity = () => {
    setOpenCity(true);
  };
  
  const updateUser = ({ email, fullName, countryId, password,cityId,street }) => {
      fetch(`${BASE_URL}/users/${id}`, { 
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email,
          fullName,
          countryId,
          cityId,
          street,
          password,
        }),
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.error) {
          setAlert(data.error);
        } else {
          setAlert(data.message);
        }
      })    
  };
 

  const update = () => {
    const classes = useStyles();
    return  (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}/>
          <Typography component="h1" variant="h5">  
            UPDATE USER
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} >
                <TextField
                  autoComplete="fullname"
                  name="fullname"
                  variant="outlined"
                  required
                  fullWidth
                  id="fullName"
                  label="Full Name"
                  onChange={(e) => setUserToUpdate({ ...userToUpdate, fullName: e.target.value })} value={userToUpdate.fullName} type="text" placeholder="fullName"
                  autoFocus
                />  
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  onChange={(e) => setUserToUpdate({ ...userToUpdate, email: e.target.value })} value={userToUpdate.email} type="text" placeholder="email"
                  autoComplete="email"
                />
              </Grid>

              <Grid item xs={12}>
              <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="Countries">Country</InputLabel>
        <Select
          labelId="Countries"
          id="Countries"
          open={openCountry}
          onClose={handleCloseCountry}
          onOpen={handleOpenCountry}
          onChange={(e) => setUserToUpdate({ ...userToUpdate, countryId: e.target.value })} value={userToUpdate.countryId} type="text" placeholder="country"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {countries.length 
          ? countries.map(country => { return <MenuItem  key={country.id} value={country.id}>{country.name}</MenuItem> })
          : null}
        </Select>

      </FormControl>
    </div>
              </Grid> 

              <Grid item xs={12}>
              <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="city">City</InputLabel>
        <Select
          labelId="Cities"
          id="Cities"
          open={openCity}
          onClose={handleCloseCity}
          onOpen={handleOpenCity}
          onChange={(e) => setUserToUpdate({ ...userToUpdate, cityId: e.target.value })} value={userToUpdate.cityId} type="text" placeholder="city"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
         
          {cities.length 
          ? cities.map(city => { return <MenuItem  key={city.id} value={city.id}>{city.city_name}</MenuItem> })
          : null}
        </Select>
      </FormControl>
    </div>
              </Grid> 
          
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={(e) => setUserToUpdate({ ...userToUpdate, street: e.target.value })} value={userToUpdate.street} type="text" placeholder="street"
                  id="street"
                  label="Address"
                  name="street"
                  autoComplete="Street"
                />
              </Grid>
            </Grid>
            <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={() => updateUser(userToUpdate)}
          >
            UPDATE
          </Button>
            
          </form>
        </div>
        <Box mt={5}>
        </Box>
      </Container>
      
    );
  };
  
  return (
    <div className="update-container">
      <br/>
      <FormControl >
        {update()}
      </FormControl>
      
      <br/>

      {alert
        ? <Error message={alert}></Error>
        : null
      }
    </div>
  );
  };


export default updateUser;


 