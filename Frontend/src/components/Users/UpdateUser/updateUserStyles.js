import {
  makeStyles
} from '@material-ui/core/styles';


export const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    button: {
      display: 'block',
      marginTop: theme.spacing(),
    },
  },
  formControl: {
    margin: theme.spacing(),
    minWidth: '100px',
    maxWidth: '300px',
  },
  container:{
    backgroundColor: 'white',
    borderRadius: '10px',
    border: '4px solid royalblue',
  },
  containerDark:{
    backgroundColor: 'darkgrey',
    borderRadius: '10px',
    border: '4px solid royalblue',
  },
}));