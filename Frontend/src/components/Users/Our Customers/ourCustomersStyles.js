import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paperDark: {
    width: '100%',
    marginBottom: theme.spacing(3),
    backgroundColor: 'darkgrey',
    borderRadius: '10px',
    border: '4px solid royalblue',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(3),
    backgroundColor: 'white',
    borderRadius: '10px',
    border: '4px solid royalblue',
  },
  table: {
    minWidth: 700,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  usersContainer:{
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: theme.spacing(4,2,2,2)
  },
  title: {
    width: '200px',
    height: '50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    '&:hover': {
      backgroundColor: 'rgba(65, 105, 225, 0.5)',
      borderRadius: '10px',
      width: '200px',
      height: '50px',
    },
    '&:active': {
      backgroundColor: 'rgba(65, 105, 225, 0.8)',
      borderRadius: '10px',
    }
  },
  error: {
    margin: theme.spacing(0, 2, 2, 2),
  },
}));

export default useStyles;