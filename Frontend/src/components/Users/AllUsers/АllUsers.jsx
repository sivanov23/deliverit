/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect, useRef, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { 
  Select, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TablePagination, 
  TableRow, 
  TableSortLabel, 
  Menu, 
  MenuItem, 
  IconButton, 
  ListItemIcon, 
  ListItemText, 
  Paper, 
  Checkbox, 
  FormControl  
} from '@material-ui/core';
import { Icon } from '@iconify/react';
import editFill from '@iconify/icons-eva/edit-fill';
import moreVerticalFill from '@iconify/icons-eva/more-vertical-fill';
import trash2Outline from '@iconify/icons-eva/trash-2-outline';
import TextField from '@material-ui/core/TextField';
import { deleteUser, getAllUsers, getAllUsersByEmail, getAllUsersByEmailAndName, getAllUsersByName } from '../../../requests/UserRequests';
import useStyles from './allUsersStyles';
import { AuthContext } from '../../../providers/AuthProvider';
import { InputLabel } from '@material-ui/core';
import { Button } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import Error from '../../Base/Error/Error';

const AllUsers = () => {
  const [allUsers, setAllUsers] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [error, setError] = useState(false);
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const { darkTheme } = useContext(AuthContext);
  const [searchParam, setSearchParam] = React.useState('');
  const [searchParamValue, setSearchParamValue] = React.useState('');
  const history = useHistory();



  useEffect(() => {
    getAllUsers()
      .then((result) => {
        if (result.data) {
          setAllUsers(result.data);
        } else {
          setAllUsers([]);
        }
      });
  }, []);

  const classes = useStyles();

  const handleSearchButton = () => {

    if (searchParam === 'name') {
      getAllUsersByName(searchParamValue)
        .then((result) => {
          if (result.data) {
            setAllUsers(result.data);
          } else {
            setAllUsers([]);
          }
        }, [])
    } else {
      if (searchParam === 'email') {
        getAllUsersByEmail(searchParamValue)
          .then((result) => {
            if (result.data) {
              setAllUsers(result.data);
            } else {
              setAllUsers([]);
            }
          }, [])
      } else {
        if (searchParam === 'search') {
          getAllUsersByEmailAndName(searchParamValue)
            .then((result) => {
              if (result.data) {
                setAllUsers(result.data);
              } else {
                setAllUsers([]);
              }
            }, [])
          return;
        }
      }
    }

    if (searchParam === '') {
      setError(true);
    }
  }

  const handleDeleteUser = (userId) => {
    if (userId.length) {
      userId.forEach(uId => {
        deleteUser(uId)
          .then(result => {
            if (result.error) {
              setError(result.error);
            } else {
              if (result.data.allUsers === null) {
                setAllUsers([]);
                setError("User not found!");
              }
              setAllUsers(result.data.allUsers);
            }
          });
      })
    }

    setTimeout(() => getAllUsers()
      .then((result) => {
        if (result.data) {
          setAllUsers(result.data);
        } else {
          setAllUsers([]);
        }
      }), 500);

  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleChange = (event) => {
    setSearchParam(event.target.value);
    setError(false);
  };

  const handleEditUser = (userId) => {
    if (userId.length !== 1) {
      setError("You can only select one user to edit")
    } else {
      history.push(`/users/update/${userId[0]}`);
    }
  }

  const rows = allUsers.map((user) => ({ 
    userId: user.id, 
    fullName: user.full_name, 
    email: user.email, 
    country_id: user.country, 
    city_id: user.city, 
    street: user.street 
  }));

  const headCells = [
    { id: 'userId', numeric: false, disablePadding: true, label: 'ID' },
    { id: 'fullName', numeric: false, disablePadding: false, label: 'Full Name' },
    { id: 'email', numeric: true, disablePadding: false, label: 'Email' },
    { id: 'country', numeric: true, disablePadding: false, label: 'Country' },
    { id: 'city', numeric: true, disablePadding: false, label: 'City' },
    { id: 'street', numeric: true, disablePadding: false, label: 'Street' },
    { id: '' }
  ];

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const UserMoreMenu = () => {
    const ref = useRef(null);
    const [isOpen, setIsOpen] = useState(false);
    return (
      <>
        <IconButton ref={ref} onClick={() => setIsOpen(true)}>
          <Icon icon={moreVerticalFill} width={20} height={20} />
        </IconButton>
        <Menu
          open={isOpen}
          anchorEl={ref.current}
          onClose={() => setIsOpen(false)}
          PaperProps={{
            sx: { width: 200, maxWidth: '100%' }
          }}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        >
          <MenuItem sx={{ color: 'text.secondary' }} onClick={() => handleDeleteUser(selected)}>
            <ListItemIcon>
              <Icon icon={trash2Outline} width={24} height={24} />
            </ListItemIcon>
            <ListItemText primary="Delete" primaryTypographyProps={{ variant: 'body2' }} />
          </MenuItem>
          <MenuItem sx={{ color: 'text.secondary' }} onClick={() => handleEditUser(selected)}>
            <ListItemIcon>
              <Icon icon={editFill} width={24} height={24} />
            </ListItemIcon>
            <ListItemText primary="Edit" primaryTypographyProps={{ variant: 'body2' }} />
          </MenuItem>
        </Menu>
      </>
    );
  }
  
  const EnhancedTableHead = (props) => {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ 'aria-label': 'select all users' }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={headCell.numeric ? 'right' : 'left'}
              padding={headCell.disablePadding ? 'none' : 'normal'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }

  const SearchField = () => {
    return (
      <div style={{ width: 300 }}>
            <TextField
              label="Search input"
              margin="normal"
              variant="outlined"
              onChange={(e) => setSearchParamValue(e.target.value)}
              value={searchParamValue}
              type="text"
              placeholder="searchParamValue"
            />
      </div>
      )};

  const EnhancedTable = () => {
    const handleRequestSort = (event, property) => {
      const isAsc = orderBy === property && order === 'asc';
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
      if (event.target.checked) {
        const newSelecteds = rows.map((n) => n.userId);
        setSelected(newSelecteds);
        setError(null);
        return;
      }
      setSelected([]);
      setError(null);
    };

    const handleClick = (event, userId) => {
      const selectedIndex = selected.indexOf(userId);
      let newSelected = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, userId);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }
      setSelected(newSelected);
      setError(null);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const isSelected = (userId) => selected.indexOf(userId) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);


    return (
      <div className={classes.root}>
        <Paper className={darkTheme ? classes.paperDark : classes.paper}>
          <div className={classes.searchFields}>
            <FormControl className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">SEARCH BY</InputLabel>
              <Select
                labelId="demo-controlled-open-select-label"
                id="demo-controlled-open-select"
                open={open}
                onClose={handleClose}
                onOpen={handleOpen}
                value={searchParam}
                onChange={handleChange}
              >
                <MenuItem value={'email'}>Email</MenuItem>
                <MenuItem value={'name'}>Name</MenuItem>
                <MenuItem value={'search'}>{`Email & Name`}</MenuItem>
              </Select>
            </FormControl>
            <FormControl >
              {SearchField()}
            </FormControl>
            <Button className={classes.searchButton}
              variant="contained"
              color="primary"
              startIcon={<SearchIcon />}
              onClick={handleSearchButton}>SEARCH</Button>
          </div>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size='medium'
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(row.userId);
                    const labelId = `enhanced-table-checkbox-${index}`;
                    
                    return (
                      <TableRow
                        hover
                        onChange={(event) => handleClick(event, row.userId)}
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={row.userId}
                        selected={isItemSelected}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                        </TableCell>
                        <TableCell component="th" id={labelId} scope="row" padding="none" align="left">
                          {row.userId}
                        </TableCell>
                        <TableCell align="left">{row.fullName}</TableCell>
                        <TableCell align="right">{row.email}</TableCell>
                        <TableCell align="right">{row.country_id}</TableCell>
                        <TableCell align="right">{row.city_id}</TableCell>
                        <TableCell align="right">{row.street}</TableCell>
                        <TableCell align="right">
                          <UserMoreMenu />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </div>
    );
  }

  return (
    <div className={classes.usersContainer}>
      <h2 className={classes.title}>ALL USERS</h2>
      {error ? <Error message='You must select search criteria!' /> : null}
      <br />
      <FormControl >
        {EnhancedTable()}
      </FormControl>
      <br />
    </div>
  );
};

export default AllUsers;
