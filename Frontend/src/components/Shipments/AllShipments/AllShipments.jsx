import React, { useEffect, useState, useRef, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TableSortLabel } from '@material-ui/core';
import { Menu, MenuItem, IconButton, ListItemIcon, ListItemText, Paper, Checkbox, FormControl } from '@material-ui/core';
import { Icon } from '@iconify/react';
import editFill from '@iconify/icons-eva/edit-fill';
import moreVerticalFill from '@iconify/icons-eva/more-vertical-fill';
import trash2Outline from '@iconify/icons-eva/trash-2-outline';
import { AuthContext } from '../../../providers/AuthProvider';
import {getAllShipments, deleteShipment} from '../../../requests/ShipmentRequests';
import { shipmentStatus } from '../../../common/shipment-status';
import { convertFromUTCTime } from '../../../utils/date';
import useStyles from './AllShipmentsStyle';
import Error from '../../../components/Base/Error/Error';

const AllShipments = () => {
  const [allShipments, setAllShipments] = useState([]);
  const [error, setError] = useState(null);
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const { darkTheme } = useContext(AuthContext);

  const history = useHistory();

  useEffect(() => {
    getAllShipments()
      .then((result) => {
        if (result.data) {
          setAllShipments(result.data);
        } else {
          setAllShipments([]);
        }
      });
  }, []);

  const classes = useStyles();

  const handleDeleteShipment = (parcelIds) => {
    if (parcelIds.length) {
      parcelIds.forEach(pId => {
        deleteShipment(pId)
          .then(result => {
            if (result.error) {
              setError(result.error);
            } else {
              if (result.data.allShipments === null) {
                setAllShipments([]);
                setError("Shipment not found!");
              }
              setAllShipments(result.data.allShipments);
            }
          });
      })
    };

    getAllShipments()
      .then((result) => {
        if (result.data) {
          setAllShipments(result.data);
        } else {
          setAllShipments([]);
        }
      });
  };

  const createShipment = () => {
    history.push('/shipments/new')
  }

  const handleEditShipment = (shipmentIds) => {
    if(shipmentIds.length !== 1){
      setError("You can only select one shipment to edit")
    } else {
      history.push(`/shipments/update/${shipmentIds[0]}`);
    }
  };

  const rows = allShipments.map((shipment) =>
    ({ shipmentId: shipment.id, 
      originWarehouse: shipment.origin_warehouse, 
      destinationWarehouse:shipment.destination_warehouse, 
      status: shipment.status, 
      departureDate: shipment.departure_date, 
      arrivalDate: shipment.arrival_date, 
    }));

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const headCells = [
    { id: 'shipmentId', numeric: false, disablePadding: false, label: 'ID' },
    { id: 'originWarehouse', numeric: false, disablePadding: false, label: 'Origin Warehouse' },
    { id: 'destinationWarehouse', numeric: false, disablePadding: false, label: 'Destination Warehouse' },
    { id: 'status', numeric: true, disablePadding: false, label: 'Status' },
    { id: 'departureDate', numeric: false, disablePadding: false, label: 'Departure Date' },
    { id: 'arrivalDate', numeric: false, disablePadding: false, label: 'Arrival Date' },
    { id: '' }
  ];

  const EnhancedTableHead = (props) => {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ 'aria-label': 'select all shipments' }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={'center'}
              padding={'normal'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }

  const EnhancedTable = () => {
    const handleRequestSort = (event, property) => {
      const isAsc = orderBy === property && order === 'asc';
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
      if (event.target.checked) {
        const newSelecteds = rows.map((n) => n.shipmentId);
        setSelected(newSelecteds);
        setError(null);
        return;
      }
      setSelected([]);
      setError(null);
    };

    const handleClick = (event, shipmentId) => {
      const selectedIndex = selected.indexOf(shipmentId);
      let newSelected = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, shipmentId);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }
      setSelected(newSelected);
      setError(null);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const isSelected = (shipmentId) => selected.indexOf(shipmentId) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
      <div className={classes.root}>
        <Paper className={darkTheme ? classes.paperDark : classes.paper}>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size='medium'
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(row.shipmentId);
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        onChange={(event) => handleClick(event, row.shipmentId)}
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={row.shipmentId}
                        selected={isItemSelected}

                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ 'aria-labelledby': labelId }}
                          />
                        </TableCell>
                        <TableCell component="th" id={labelId} scope="row" padding="none" align="center">
                          {row.shipmentId}
                        </TableCell>
                        <TableCell align="center">{row.originWarehouse}</TableCell>
                        <TableCell align="center">{row.destinationWarehouse}</TableCell>
                        <TableCell align="center">{shipmentStatus[row.status]}</TableCell>
                        <TableCell align="center">{convertFromUTCTime(row.departureDate)}</TableCell>
                        <TableCell align="center">{convertFromUTCTime(row.arrivalDate)}</TableCell>
                        <TableCell align="center">
                          <MoreMenu />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </div>
    );
  };

  const MoreMenu = () => {
    const ref = useRef(null);
    const [isOpen, setIsOpen] = useState(false);

    return (
      <>
        <IconButton ref={ref} onClick={() => setIsOpen(true)}>
          <Icon icon={moreVerticalFill} width={20} height={20} />
        </IconButton>
        <Menu
          open={isOpen}
          anchorEl={ref.current}
          onClose={() => setIsOpen(false)}
          PaperProps={{
            sx: { width: 200, maxWidth: '100%' }
          }}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        >
          <MenuItem sx={{ color: 'text.secondary' }} onClick={() => handleDeleteShipment(selected)}>
            <ListItemIcon>
              <Icon icon={trash2Outline} width={24} height={24} />
            </ListItemIcon>
            <ListItemText primary="Delete" primaryTypographyProps={{ variant: 'body2' }} />
          </MenuItem>
          <MenuItem sx={{ color: 'text.secondary' }} onClick={() => handleEditShipment(selected)}>
            <ListItemIcon>
              <Icon icon={editFill} width={24} height={24} />
            </ListItemIcon>
            <ListItemText primary="Edit" primaryTypographyProps={{ variant: 'body2' }} />
          </MenuItem>
        </Menu>
      </>
    );
  }

  return (
    <div className={classes.shipmentsContainer}>
      <h2 className={classes.title}>ALL SHIPMENTS</h2>
      <br />
      {error? 
        <div className={classes.error}>
        <Error message={error}/>
      </div> : null}
      <FormControl >
        {EnhancedTable()}
      </FormControl>
      <Button className={classes.createButton} variant="contained" color="primary" onClick={createShipment}>Create shipment</Button>
      <br />
    </div>
  );
};

export default AllShipments;