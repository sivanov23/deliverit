import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Button,
  TextField,
  Box,
  Typography,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select
} from '@material-ui/core';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import { useStyles } from './createShipmentStyles';
import { AuthContext } from '../../../providers/AuthProvider';
import { createShipment } from '../../../requests/ShipmentRequests';
import { getWarehouses } from '../../../requests/WarehouseRequests';
import { shipmentStatus } from '../../../common/shipment-status';

const CreateShipment = () => {
  const history = useHistory();
  const { darkTheme } = useContext(AuthContext);

  const [warehouses, setWarehouses] = useState([]);
  const [shipmentToCreate, setShipmentToCreate] = useState({
    originWarehouseId: "",
    destinationWarehouseId: "",
    status: "",
    arrivalDate: "",
    departureDate: ""
  });

  useEffect(() => {
    getWarehouses()
      .then((result) => {
        setWarehouses(result.data)
      });
  }, []);

  const createNewShipment = ({ originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate }) => {
    createShipment({ originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate });
    history.push('/shipments');
  };

  const addOriginWarehouse = (e) => {
    setShipmentToCreate({...shipmentToCreate, originWarehouseId: e.target.value});
  }

  const addDestinationWarehouse = (e) => {
    setShipmentToCreate({ ...shipmentToCreate, destinationWarehouseId: e.target.value });
  }

  const addStatus = (e) => {
    setShipmentToCreate({ ...shipmentToCreate, status: e.target.value });
  }

  const addDepartureDate = (e) => {
    setShipmentToCreate({ ...shipmentToCreate, departureDate: e.target.value });
    console.log(shipmentToCreate);
  }

  const addArrivalDate = (e) => {
    setShipmentToCreate({...shipmentToCreate, arrivalDate: e.target.value});
    console.log(shipmentToCreate);
  }

  const classes = useStyles();

  const CreateNewShipmentJSX = () => {
    return (
      <Container className={darkTheme ? classes.containerDark : classes.container} component="main" maxWidth="xs">
        <div className={classes.paper}>
          <div className={classes.iconBackground}>
            <LocalShippingIcon className={classes.avatar} />
          </div>
          <Typography component="h1" variant="h5">
            Create Shipment
          </Typography>
              <div className={classes.neatForm}>
                <div className={classes.formElement}>
                  <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel htmlFor="Origin Warehouse">Origin Warehouse</InputLabel>
                    <Select
                      label="Origin Warehouse"
                      id="Origin Warehouse"
                      onChange={addOriginWarehouse}
                      value={shipmentToCreate.originWarehouseId}
                      type="text"
                      placeholder="Origin Warehouse"
                    >
                      {warehouses.length
                        ? warehouses.map(warehouse => { return <MenuItem key={warehouse.id} value={warehouse.id}>{`${warehouse.street}, ${warehouse.city}, ${warehouse.country}`}</MenuItem> })
                        : null}
                    </Select>
                  </FormControl>
                </div>
                <div className={classes.formElement}>
                  <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel htmlFor="Destination Warehouse">Destination Warehouse</InputLabel>
                    <Select
                      label="Destination Warehouse"
                      id="Destination Warehouse"
                      onChange={addDestinationWarehouse}
                      value={shipmentToCreate.destinationWarehouseId}
                      type="text"
                      placeholder="Destination Warehouse"
                    >
                      {warehouses.length
                        ? warehouses.map(warehouse => { return <MenuItem key={warehouse.id} value={warehouse.id}>{`${warehouse.street}, ${warehouse.city}, ${warehouse.country}`}</MenuItem> })
                        : null}
                    </Select>
                  </FormControl>
                </div>
                <div className={classes.formElement}>
                  <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel htmlFor="Status">Status</InputLabel>
                    <Select
                      label="Status"
                      id="Status"
                      onChange={addStatus}
                      value={shipmentToCreate.status}
                      type="text"
                      placeholder="Status"
                    >
                     <MenuItem value={1}>{`${shipmentStatus[1]}`}</MenuItem>
                     <MenuItem value={2}>{`${shipmentStatus[2]}`}</MenuItem>
                     <MenuItem value={3}>{`${shipmentStatus[3]}`}</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                 <div className={classes.formElement}>
                  <form className={classes.calendar} noValidate>
                    <TextField
                      id="departure-date"
                      label="Departure Date"
                      type="datetime-local"
                      value={shipmentToCreate.departureDate}
                      onChange={addDepartureDate}
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </form>
                </div>
                <div className={classes.formElement}>
                  <form className={classes.calendar} noValidate>
                    <TextField
                      id="arrival-date"
                      label="Arrival Date"
                      type="datetime-local"
                      value={shipmentToCreate.arrivalDate}
                      onChange={addArrivalDate}
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </form>
                </div>
                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  onClick={() => createNewShipment(shipmentToCreate)}
                >
                  Create Shipment
                </Button>
              </div>
          </div>
        <Box mt={5}>
        </Box>
      </Container>

    );
  };

  return (
    <div className={classes.shipmentsContainer}>
      <h2 className={classes.title}>CREATE SHIPMENT</h2>
      <br />
      <FormControl >
        {CreateNewShipmentJSX()}
      </FormControl>
      <br />
    </div>
  );
};

export default CreateShipment;