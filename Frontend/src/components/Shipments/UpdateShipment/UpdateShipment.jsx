import React, { useState, useEffect, useContext } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import {
  Button,
  TextField,
  Box,
  Typography,
  Container,
  FormControl,
  MenuItem,
  Select
} from '@material-ui/core';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import moment from 'moment';
import { useStyles } from './updateShipmentStyles';
import { AuthContext } from '../../../providers/AuthProvider';
import { getShipmentById, updateShipment } from '../../../requests/ShipmentRequests';
import { getWarehouses } from '../../../requests/WarehouseRequests';
import { shipmentStatus } from '../../../common/shipment-status';

const UpdateShipment = () => {
  const history = useHistory();
  const {id} = useParams();
  const { darkTheme } = useContext(AuthContext);

  const [warehouses, setWarehouses] = useState([]);
  const [shipmentToUpdate, setShipmentToUpdate] = useState({
    originWarehouseId: '',
    destinationWarehouseId: '',
    status: '',
    arrivalDate: new Date(),
    departureDate: new Date(),
  });

  useEffect(() => {
    getWarehouses()
      .then((result) => {
        setWarehouses(result.data);
      });

    getShipmentById(id)
      .then(result => {
        setShipmentToUpdate({
          originWarehouseId: result.data.origin_warehouse,
          destinationWarehouseId: result.data.destination_warehouse,
          status: result.data.status,
          arrivalDate: result.data.arrival_date,
          departureDate: result.data.departure_date,
        })
     })
  }, [id]);

  const submitUpdateShipment = ({ originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate }, id) => {
    updateShipment({ originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate }, id);
    history.push('/shipments');
  };

  const addOriginWarehouse = (e) => {
    setShipmentToUpdate({ ...shipmentToUpdate, originWarehouseId: e.target.value });
  }

  const addDestinationWarehouse = (e) => {
    setShipmentToUpdate({ ...shipmentToUpdate, destinationWarehouseId: e.target.value });
  }

  const addStatus = (e) => {
    setShipmentToUpdate({ ...shipmentToUpdate, status: e.target.value });
  }

  const addDepartureDate = (e) => {
    setShipmentToUpdate({ ...shipmentToUpdate, departureDate: e.target.value });
    console.log(shipmentToUpdate);
  }

  const addArrivalDate = (e) => {
    setShipmentToUpdate({ ...shipmentToUpdate, arrivalDate: e.target.value });
    console.log(shipmentToUpdate);
  }

  const classes = useStyles();

  const submitUpdateShipmentJSX = () => {
    return (
      <Container className={darkTheme ? classes.containerDark : classes.container} component="main" maxWidth="xs">
        <div className={classes.paper}>
          <div className={classes.iconBackground}>
            <LocalShippingIcon className={classes.avatar} />
          </div>
          <Typography component="h1" variant="h5">
            Update Shipment
          </Typography>
           
            <div className={classes.neatForm}>
            <div className={classes.formElement}>
                  <FormControl variant="outlined" className={classes.formControl}>
                    <Select
                      id="Origin Warehouse"
                      onChange={addOriginWarehouse}
                      value={shipmentToUpdate.originWarehouseId}
                      type="text"
                      placeholder="Origin Warehouse"
                    >
                      {warehouses.length
                        ? warehouses.map(warehouse => { return <MenuItem key={warehouse.id} value={warehouse.id}>{`${warehouse.street}, ${warehouse.city}, ${warehouse.country}`}</MenuItem> })
                        : null}
                    </Select>
                  </FormControl>
                </div>


            <div className={classes.formElement}>
                  <FormControl variant="outlined" className={classes.formControl}>
                    <Select
                      id="Destination Warehouse"
                      onChange={addDestinationWarehouse}
                      value={shipmentToUpdate.destinationWarehouseId}
                      type="text"
                      placeholder="Destination Warehouse"
                    >
                      {warehouses.length
                        ? warehouses.map(warehouse => { return <MenuItem key={warehouse.id} value={warehouse.id}>{`${warehouse.street}, ${warehouse.city}, ${warehouse.country}`}</MenuItem> })
                        : null}
                    </Select>
                  </FormControl>
                </div>


            <div className={classes.formElement}>
                  <FormControl variant="outlined" className={classes.formControl}>
                    <Select
                      id="Status"
                      onChange={addStatus}
                      value={shipmentToUpdate.status}
                      type="text"
                    >
                      <MenuItem value={1}>{`${shipmentStatus[1]}`}</MenuItem>
                      <MenuItem value={2}>{`${shipmentStatus[2]}`}</MenuItem>
                      <MenuItem value={3}>{`${shipmentStatus[3]}`}</MenuItem>
                    </Select>
                  </FormControl>
                </div>

            <div className={classes.formElement}>
                  <form className={classes.calendar} noValidate>
                    <TextField
                      id="departure-date"
                      label="Departure Date"
                      type="datetime-local"
                      value={moment(shipmentToUpdate.departureDate).format("YYYY-MM-DDTkk:mm")}
                      onChange={addDepartureDate}
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </form>
                </div>

            <div className={classes.formElement}>
                  <form className={classes.calendar} noValidate>
                    <TextField
                      id="arrival-date"
                      label="Arrival Date"
                      type="datetime-local"
                      value={moment(shipmentToUpdate.arrivalDate).format("YYYY-MM-DDTkk:mm")}
                      onChange={addArrivalDate}
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </form>
                </div>
          </div>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => submitUpdateShipment(shipmentToUpdate, id)}
            >
              Update shipment
            </Button>
        </div>
        <Box mt={5}>
        </Box>
      </Container>
    );
  };

  return (
    <div className={classes.shipmentsContainer}>
      <h2 className={classes.title}>UPDATE SHIPMENTS</h2>
      <br />
      <FormControl >
        {submitUpdateShipmentJSX()}
      </FormControl>
      <br />
    </div>
  );
};

export default UpdateShipment;