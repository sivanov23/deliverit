import {
  makeStyles
} from '@material-ui/core/styles';


export const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    fontSize: '50px',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    button: {
      display: 'block',
      marginTop: theme.spacing(),
    },
  },
  formControl: {
    margin: theme.spacing(),
    width: '300px',
  },
  container: {
    backgroundColor: 'white',
    borderRadius: '10px',
    border: '4px solid royalblue',
    width: '600px',
  },
  containerDark: {
    backgroundColor: 'darkgrey',
    borderRadius: '10px',
    border: '4px solid royalblue',
    width: '600px',
  },
  calendar: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  shipmentsContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: theme.spacing(2),
  },
  title: {
    width: '250px',
    height: '50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    '&:hover': {
      backgroundColor: 'rgba(65, 105, 225, 0.5)',
      borderRadius: '10px',
    },
    '&:active': {
      backgroundColor: 'rgba(65, 105, 225, 0.8)',
      borderRadius: '10px',
    },
  },
  neatForm: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  formElement: {
    margin: theme.spacing(1, 0),
  },
  iconBackground: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '65px',
    borderRadius: '32px',
    backgroundColor: theme.palette.secondary.main,
    marginBottom: theme.spacing(2),
  }
}));