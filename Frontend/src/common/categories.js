export const parcelCategories = {
  1: "ELECTRONICS",
  2: "FASHION",
  3: "HEALTH AND BEAUTY",
  4: "HOME AND GARDEN",
  5: "SPORTS",
  6: "COLLECTIBLES AND ART",
  7: "INDUSTRIAL EQUIPMENT",
  8: "MOTORS",
};
