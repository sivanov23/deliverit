export const shipmentStatus = {
  1: "PREPARING" ,
  2: "ON THE WAY",
  3: "COMPLETED",
};
