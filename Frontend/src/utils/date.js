export const convertFromUTCTime = (utcString) => {
  const date = new Date(utcString);
  return date.toLocaleDateString();
}