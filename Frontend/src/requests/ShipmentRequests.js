import { BASE_URL } from "../common/constants";

export const getAllShipments = async () => {
  return fetch(`${BASE_URL}/shipments/`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const getShipmentById = async (id) => {
  return fetch(`${BASE_URL}/shipments/${id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const getAllShipmentsByStatus = async () => {
  return fetch(`${BASE_URL}/shipments/status`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const getAllShipmentsByUserId = async (id) => {
  return fetch(`${BASE_URL}/shipments?customerId=${id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const createShipment = async ({ originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate}) => {
  return fetch(`${BASE_URL}/shipments/`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      originWarehouseId,
      destinationWarehouseId,
      status,
      arrivalDate,
      departureDate,
    }),
  })
};

export const updateShipment = async ({ originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate }, id) => {
  return fetch(`${BASE_URL}/shipments/${id}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      originWarehouseId,
      destinationWarehouseId,
      status,
      arrivalDate,
      departureDate,
    }),
  })
};

export const deleteShipment = async(id) => {
  return fetch(`${BASE_URL}/shipments/${id}`, {
    method: "DELETE",
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": 'application/json',
    },
  }).then((response) => response.json());
};
