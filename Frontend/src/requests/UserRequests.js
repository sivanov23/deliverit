import { BASE_URL } from "../common/constants";

export const getAllUsers = async () => {
  return fetch(`${BASE_URL}/users/`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const getAllUsersByName = async (name) => {
  return fetch(`${BASE_URL}/users?name=${name}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const getAllUsersByEmail = async (email) => {
  return fetch(`${BASE_URL}/users?email=${email}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const getAllUsersByEmailAndName = async (search) => {
  return fetch(`${BASE_URL}/users?search=${search}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const deleteUser = async (id) => {
  return fetch(`${BASE_URL}/users/${id}`, {
    method: "DELETE",
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": 'application/json',
    },
  }).then((response) => response.json());
};

export const createUser = async ({
  email,
  fullName,
  countryId,
  password,
  cityId,
  street
}) => {
  return fetch(`${BASE_URL}/users/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        fullName,
        countryId,
        cityId,
        street,
        password,
      }),
    })
    .then((response) => response.json());
};

export const login = async (email, password) => {
  return fetch(`${BASE_URL}/auth/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email,
      password,
    }),
  })
    .then(r => r.json());
};

export const getIncomingParcels = async (id) => {
  return fetch(`${BASE_URL}/users/${id}/parcels/`, {
      method: "Get",
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        "Content-Type": "application/json",
      },
    })
    .then((response) => response.json())
};

export const updateUser = async ({email,fullName, countryId, cityId, street},id) => {
  fetch(`${BASE_URL}/users/${id}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email,
      fullName,
      countryId,
      cityId,
      street,
    }),
  })
    .then((response) => response.json())

}