import { BASE_URL } from "../common/constants";

export const postWarehouse = async ({street, city, country, latitude, longitude}) => {
  return fetch(`${BASE_URL}/warehouses`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ street, city, country, latitude, longitude})
  }).then(response => response.json());
}

export const getWarehouses = async () => {
  return fetch(`${BASE_URL}/warehouses`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  }).then(response => response.json());
}

export const getSingleWarehouse = async (id) => {
  return fetch(`${BASE_URL}/warehouses/${id}`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
  }).then(response => response.json());
}

export const putWarehouse = async ({ street, city, country, latitude, longitude }, id) => {
  return fetch(`${BASE_URL}/warehouses/${id}`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ street, city, country, latitude, longitude })
  }).then(response => response.json());
}

export const deleteWarehouse = async (id) => {
  return fetch(`${BASE_URL}/warehouses/${id}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
  }).then(response => response.json());
}
