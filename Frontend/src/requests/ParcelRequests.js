import { BASE_URL } from "../common/constants";

export const getAllParcels = async () => {
  return fetch(`${BASE_URL}/parcels/`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
};

export const getParcelsByUserId = async (id) => {
  return fetch(`${BASE_URL}/parcels?customerId=${id}`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json());
};

export const deleteParcel = async (id) => {
  return fetch(`${BASE_URL}/parcels/${id}`, {
    method: "DELETE",
    headers: {
      "Authorization": `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": 'application/json',
    },
  }).then((response) => response.json());
};

export const putParcel = async ({ weight, category, shipmentId, deliverToAddress,userId }, id) => {
  return fetch(`${BASE_URL}/parcels/${id}`, {
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ weight, category, shipmentId, deliverToAddress,userId })
  }).then(response => response.json());
};

export const getSingleParcel = async (id) => {
  return fetch(`${BASE_URL}/parcels/?parcelId=${id}`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Content-Type': 'application/json',
    },
  }).then(response => response.json());
};

export const postParcel = async ({ weight, category, userId, deliverToAddress, shipmentId }) => {
  fetch(`${BASE_URL}/parcels/`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify({
      weight,
      category,
      userId,
      deliverToAddress,
      shipmentId
    }),
  })
    .then((response) => response.json());
};