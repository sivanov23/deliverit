import { BASE_URL } from "../common/constants";

export const getAllCountries = async () => {
  return fetch(`${BASE_URL}/countries/`, {
  method: "GET",
  headers: {
    Authorization: `Bearer ${localStorage.getItem('token')}`,
    "Content-Type": "application/json",
  },
})
  .then((response) => response.json());
};

export const getAllCitiesByCountry = async (id) => {
  return fetch(`${BASE_URL}/countries/${id}/cities`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json());
};