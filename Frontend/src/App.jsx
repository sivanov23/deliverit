import React, { useState } from "react";
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import getUserFromToken from './utils/token';
import Login from "./components/Auth/Login/Login";
import Header from "./components/Base/Header/Header";
import Home from "./components/Base/Home/Home";
import { AuthContext } from "./providers/AuthProvider";
import Register from "./components/Auth/Registration/Registration";
import GuardedRoute from "./providers/GuardedRoute";
import Container from "./components/Base/Container/Container";
import AllWarehouses from "./components/Warehouses/AllWarehouses/AllWarehouses";
import AllParcels from "./components/Parcels/AllParcels/AllParcels";
import CreateWarehouse from './components/Warehouses/CreateWarehouse/CreateWarehouse';
import EditWarehouse from './components/Warehouses/EditWarehouse/EditWarehouse';
import MyParcels from "./components/Parcels/MyOrders/MyOrders";
import CreateParcel from "./components/Parcels/CreateParcel/CreateParcel";
import AllUsers from "./components/Users/AllUsers/АllUsers";
import UpdateUser from "./components/Users/UpdateUser/UpdateUser";
import parcelUpdate from "./components/Parcels/UpdateParcel/UpdateParcel";
import AllShipments from "./components/Shipments/AllShipments/AllShipments";
import CompletedOrders from "./components/Parcels/CompletedOrders/CompletedOrders";
import CreateShipment from "./components/Shipments/CreateShipment/CreateShipment";
import UpdateShipment from "./components/Shipments/UpdateShipment/UpdateShipment";
import OurCustomers from "./components/Users/Our Customers/OurCustomers.jsx";

const App = () => {
  const [user, setUser] = useState(getUserFromToken(localStorage.getItem('token')));
  const [darkTheme, setDarkTheme] = useState(false);
  const isLoggedIn = !!user;

  return (
    <AuthContext.Provider value={{ user, setUser, darkTheme, setDarkTheme }}>
      <BrowserRouter>
        <div className="app">
          <Header />
          <Container>
            <Switch>
              <Redirect path="/" exact to="/home" />
              <Route exact path="/home" component={Home} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/customers" component={OurCustomers} />
              <Route exact path="/warehouses" component={AllWarehouses} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/shipments" component={AllShipments} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/shipments/new" component={CreateShipment} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/shipments/update/:id" component={UpdateShipment} />
              <GuardedRoute exact auth={isLoggedIn} path="/parcels/my/completed" component={CompletedOrders} />
              <GuardedRoute exact auth={isLoggedIn} path="/parcels/my" component={MyParcels} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/parcels" component={AllParcels} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/users" component={AllUsers} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/parcels/new" component={CreateParcel} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/parcels/update/:id" component={parcelUpdate} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/users/update/:id" component={UpdateUser} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/warehouses/new" component={CreateWarehouse} />
              <GuardedRoute exact auth={user ? user.role === 2 : false} path="/warehouses/:id" component={EditWarehouse} />
            </Switch>
          </Container>
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
};

export default App;