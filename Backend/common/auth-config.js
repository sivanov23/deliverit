import dotenv from 'dotenv';

const config = dotenv.config().parsed;

export const PRIVATE_KEY = config.SECRET_KEY;

// 7 days * 24 hours * 60 mins * 60 secs
export const TOKEN_LIFETIME = 7 * 24 * 60 * 60;
