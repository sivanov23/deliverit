-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema deliver_it
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `deliver_it` DEFAULT CHARACTER SET utf8mb3 ;
USE `deliver_it` ;

-- -----------------------------------------------------
-- Table `deliver_it`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver_it`.`countries` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver_it`.`cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver_it`.`cities` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `country_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_City_Country1_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_City_Country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `deliver_it`.`countries` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver_it`.`warehouses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver_it`.`warehouses` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `country_id` INT(11) NOT NULL,
  `city_id` INT(11) NOT NULL,
  `street` VARCHAR(256) NOT NULL,
  `latitude` DECIMAL(15,10) NULL DEFAULT NULL,
  `longitude` DECIMAL(15,10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_warehouses_cities1_idx` (`city_id` ASC) VISIBLE,
  INDEX `fk_warehouses_countries1_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_warehouses_cities1`
    FOREIGN KEY (`city_id`)
    REFERENCES `deliver_it`.`cities` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_warehouses_countries1`
    FOREIGN KEY (`country_id`)
    REFERENCES `deliver_it`.`countries` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver_it`.`shipments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver_it`.`shipments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `origin_warehouse` INT(11) NOT NULL,
  `destination_warehouse` INT(11) NOT NULL,
  `departure_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  `arrival_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  `status` TINYINT(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Shipment_Warehouse1_idx` (`origin_warehouse` ASC) VISIBLE,
  INDEX `fk_Shipment_Warehouse2_idx` (`destination_warehouse` ASC) VISIBLE,
  CONSTRAINT `fk_Shipment_Warehouse1`
    FOREIGN KEY (`origin_warehouse`)
    REFERENCES `deliver_it`.`warehouses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Shipment_Warehouse2`
    FOREIGN KEY (`destination_warehouse`)
    REFERENCES `deliver_it`.`warehouses` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver_it`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver_it`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `full_name` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `role` TINYINT(2) NOT NULL DEFAULT 1,
  `country_id` INT(11) NOT NULL,
  `city_id` INT(11) NOT NULL,
  `street` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_countries1_idx` (`country_id` ASC) VISIBLE,
  INDEX `fk_users_cities1_idx` (`city_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_cities1`
    FOREIGN KEY (`city_id`)
    REFERENCES `deliver_it`.`cities` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_countries1`
    FOREIGN KEY (`country_id`)
    REFERENCES `deliver_it`.`countries` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `deliver_it`.`parcels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `deliver_it`.`parcels` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `weight` DECIMAL(10,3) NOT NULL,
  `category` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `shipment_id` INT(11) NOT NULL,
  `deliver_to_address` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_parcel_Shipment1_idx` (`shipment_id` ASC) VISIBLE,
  INDEX `fk_parcels_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_parcel_Shipment1`
    FOREIGN KEY (`shipment_id`)
    REFERENCES `deliver_it`.`shipments` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_parcels_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `deliver_it`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
