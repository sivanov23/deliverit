import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';


const config = dotenv.config().parsed;
const PRIVATE_KEY = config.SECRET_KEY;

const options = {
  secretOrKey: PRIVATE_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const userData = {
    id: payload.id,
    fullName: payload.fullName,
    email: payload.email,
    role: payload.role,
  };

  done(null, userData);
});

export default jwtStrategy;
