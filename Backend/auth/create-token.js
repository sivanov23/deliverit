import jwt from 'jsonwebtoken';
import { PRIVATE_KEY, TOKEN_LIFETIME } from '../common/auth-config.js';

const createToken = (payload) => {
  const options = {
    expiresIn: TOKEN_LIFETIME,
  };

  const token = jwt.sign(
    payload,
    PRIVATE_KEY,
    options
  );

  return token;
};


export default createToken;
