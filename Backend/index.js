import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import dotenv from 'dotenv';
import usersRoute from './controllers/user-controller.js';
import authController from './controllers/auth-controller.js';
import locationRoute from './controllers/location-controller.js';
import parcelsRoute from './controllers/parcel-controller.js';
import warehouseRoute from './controllers/warehouse-controller.js';
import shipmentRouter from './controllers/shipment-controller.js';

const config = dotenv.config().parsed;
const app = express();

const PORT = +config.SERVER_PORT;

passport.use(jwtStrategy);

app.use(passport.initialize());
app.use(helmet());
app.use(cors());
app.use(express.json());


app.use('/auth', authController);
app.use('/users', usersRoute);
app.use('/countries', locationRoute);
app.use('/parcels', parcelsRoute);
app.use('/warehouses', warehouseRoute);
app.use('/shipments', shipmentRouter);


app.use((err, req, res, next) => {
  // logger.log(err)
  res.status(500).send({
    // eslint-disable-next-line max-len
    error: 'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});


app.all('*', (req, res) =>
  res.status(404).send({ error: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
