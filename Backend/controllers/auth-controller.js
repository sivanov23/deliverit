import express from 'express';
import createToken from '../auth/create-token.js';
import usersData from '../data/users-data.js';
import errors from '../services/errors.js';
import { logInUser } from '../services/user-service.js';


/**
 * Calls service function to log in the user and create a token for auth
 * @param {req} req request
 * @param {res} res response
 * @return {res} token for authentication or error message
 */
// eslint-disable-next-line new-cap
const authController = express.Router();

authController.post('/login', async (req, res) => {
  const { email, password } = req.body;
  const result = await logInUser(usersData)(email, password);
  if (result.error === errors.INVALID_SIGNIN || result.error === errors.NOT_FOUND) {
    res.status(400).send({
      ...result,
      error: 'Invalid email/password',
    });
  } else {
    const payload = {
      id: result.data.id,
      fullName: result.data.full_name,
      email: result.data.email,
      role: result.data.role,
    };
    const token = createToken(payload);

    res.status(200).send({
      token: token,
    });
  }
});

export default authController;


