import express from 'express';
import {
  getAllCountries,
  getAllCities,
  getCitiesByCountryId
} from '../services/location-service.js';
import locationData from '../data/location-data.js';
import errors from '../services/errors.js';

const locationRouter = express.Router();

/**
 * Calls service function to show all countries
 * @param {req} req request
 * @param {res} res response
 * @return {res} all countries data or error message
 */

locationRouter.get('/', async (req, res) => {
  const countries = await getAllCountries(locationData)();
  if (countries.error === errors.NOT_FOUND) {
    res.status(400).send({
      ...countries,
      error: 'Countries not found',
    });
  } else {
    res.status(200).send(countries);
  }
});

/**
 * Calls service function to show all cities
 * @param {req} req request
 * @param {res} res response
 * @return {res} all cities data or error message
 */

locationRouter.get('/cities', async (req, res) => {
  const cities = await getAllCities(locationData)();
  if (cities.error === errors.NOT_FOUND) {
    res.status(400).send({
      ...cities,
      error: 'Cities not found',
    });
  } else {
    res.status(200).send(cities);
  }
});

/**
 * Calls service function to show all cities from a specific country
 * @param {req} req request
 * @param {res} res response
 * @return {res} all cities data by specific country or error message
 */

locationRouter.get('/:id/cities', async (req, res) => {
  const countryId = req.params.id;
  const cities = await getCitiesByCountryId(locationData)(countryId);
  if (cities.error === errors.NOT_FOUND) {
    res.status(400).send({
      ...cities,
      error: 'Country or cities not found',
    });
  } else {
    res.status(200).send(cities);
  }
});

export default locationRouter;
