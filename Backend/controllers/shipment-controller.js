import express from 'express';
import errors from '../services/errors.js';
import validateBody from '../middlewares/validate-body.js';
import {
  authMiddleware,
} from '../auth/auth-middleware.js';
import roleValidator from '../middlewares/role-validator.js';
import createShipmentValidation from '../validators/create-shipment-validation.js';
import {
  getAllShipments,
  getAllShipmentsByCustomerId,
  getAllShipmentsByWarehouseId,
  getShipmentById,
  createShipment,
  updateShipment,
  deleteShipment,
  getAllShipmentsByStatusId,

} from '../services/shipment-service.js';
import shipmentData from '../data/shipment-data.js';

const shipmentRouter = express.Router();

shipmentRouter.use(authMiddleware);

/**
 * Calls service function to show all shipments and filter by customer and warehouse id
 * @param {req} req request
 * @param {res} res response
 * @return {res} all shipment data or error message
 */

shipmentRouter.get('/', async (req, res) => {
  const {
    warehouseId,
    customerId,
  } = req.query;
  let shipments;

  if (!warehouseId && !customerId) {
    shipments = await getAllShipments(shipmentData)();
  } else if (customerId) {
    shipments = await getAllShipmentsByCustomerId(shipmentData)(customerId);
  } else if (warehouseId) {
    shipments = await getAllShipmentsByWarehouseId(shipmentData)(warehouseId);
  }
  if (shipments.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...shipments,
      error: 'Shipments not found!',
    });
  }
  return res.status(200).json(shipments);
});

/**
 * Calls service function to show all shipments by status
 * @param {req} req request
 * @param {res} res response
 * @return {res} all shipment data or error message
 */

shipmentRouter.get('/status', async (req, res) => {
  const shipments = await getAllShipmentsByStatusId(shipmentData)();
  if (shipments.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...shipments,
      error: 'Shipments not found!',
    });
  }
  return res.status(200).json(shipments);
});

/**
 * Calls service function to show a single shipment by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} all shipment data or error message
 */

shipmentRouter.get('/:id', async (req, res) => {
  const {
    id,
  } = req.params;
  const shipment = await getShipmentById(shipmentData)(id);

  if (shipment.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...shipment,
      error: 'Shipments not found!',
    });
  }
  return res.status(200).json(shipment);
});

/**
 * Calls service function to create a shipment
 * @param {req} req request
 * @param {res} res response
 * @return {res} created shipment data or error message
 */

shipmentRouter.post('/', validateBody(createShipmentValidation), roleValidator, async (req, res) => {
  const {
    originWarehouseId,
    destinationWarehouseId,
    status,
    arrivalDate,
    departureDate,
  } = req.body;
  const createdShipment = await createShipment(shipmentData)(originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate);

  if (createdShipment.error === errors.OPERATION_FAILED) {
    return res.status(400).json({
      ...createdShipment,
      error: 'Failed to create shipment',
    });
  }
  return res.status(201).json(createdShipment);
});

/**
 * Calls service function to update a parcel by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} updated parcel data or error message
 */

shipmentRouter.put('/:id', validateBody(createShipmentValidation), roleValidator, async (req, res) => {
  const shipmentId = req.params.id;
  const body = req.body;
  const updatedShipment = await updateShipment(shipmentData)(shipmentId, body);

  if (updatedShipment.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...updatedShipment,
      error: 'Shipment with that ID not found!',
    });
  }
  return res.status(200).json(updatedShipment);
});

/**
 * Calls service function to delete a shipment by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} deleted shipment data or error message
 */

shipmentRouter.delete('/:id', roleValidator, async (req, res) => {
  const shipmentId = req.params.id;
  const deletedShipment = await deleteShipment(shipmentData)(shipmentId);

  if (deletedShipment.error === errors.NOT_FOUND) {
    return res.status(400).json({
      ...deletedShipment,
      error: 'Shipment with that ID not found!',
    });
  }
  return res.sendStatus(204);
});

export default shipmentRouter;
