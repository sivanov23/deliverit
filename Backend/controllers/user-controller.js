/* eslint-disable max-len */
/* eslint-disable linebreak-style */
import express from 'express';
import validateBody from '../middlewares/validate-body.js';
import {
  getUsers,
  getUsersByEmail,
  getUsersByName,
  searchUser,
  getIncomingParcels,
  registerUser,
  updateUser,
  deleteUser,
} from '../services/user-service.js';
import createUserValidator from '../validators/create-user-validator.js';
import updateUserValidator from '../validators/update-user-validator.js';
import errors from '../services/errors.js';
import {
  authMiddleware
} from '../auth/auth-middleware.js';
import roleValidator from '../middlewares/role-validator.js';
import usersData from '../data/users-data.js';

const usersRoute = express.Router();


/**
 * Calls service function to show all users and filter by email and name or both
 * @param {req} req request
 * @param {res} res response
 * @return {res} all users data or error message
 */

usersRoute.get('/', async (req, res) => {
  const {
    name,
    email,
    search
  } = req.query;
  let users;
  if (search) {
    users = await searchUser(usersData)(search);
  } else if (name) {
    users = await getUsersByName(usersData)(name);
  } else if (email) {
    users = await getUsersByEmail(usersData)(email);
  } else {
    users = await getUsers(usersData)();
  }

  if (users.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...users,
      error: `Customer not found!`,
    });
  }
  if (users.error === errors.FORBIDDEN) {
    return res.status(403).json({
      ...users,
      error: `Sorry, you don't have access to other customers' data!`,
    });
  }
  return res.status(200).json(users);
});

/**
 * Calls service function to show all incoming parcels by user
 * @param {req} req request
 * @param {res} res response
 * @return {res} all parcels data or error message
 */

usersRoute.get('/:id/parcels', authMiddleware, async (req, res) => {
  const userId = +req.params.id;
  const currentUser = req.user;
  const parcels = await getIncomingParcels(usersData)(userId, currentUser);

  if (parcels.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...parcels,
      error: 'Incoming parcels not found!',
    });
  } else if (parcels.error === errors.FORBIDDEN) {
    return res.status(403).json({
      ...parcels,
      error: 'You are not authorized for this request!',
    });
  }

  return res.status(200).json(parcels);
});

/**
 * Calls service function to register user
 * @param {req} req request
 * @param {res} res response
 * @return {res} registered user data or error message
 */

usersRoute.post('/', validateBody(createUserValidator), async (req, res) => {
  const registeredUser = await registerUser(usersData)(req.body);

  if (registeredUser.error === errors.DUPLICATE_RECORD) {
    return res.status(400).json({
      ...registeredUser,
      error: `Customer with email ${req.body.email} already exists!`,
    });
  }

  return res.status(201).json(registeredUser);
});

/**
 * Calls service function to update user information by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} updated user data or error message
 */

usersRoute.put('/:id', authMiddleware, validateBody(updateUserValidator), async (req, res) => {
  const userId = +req.params.id;
  const updatedUser = await updateUser(usersData)(userId, req.body);

  if (updatedUser.error === errors.NOT_FOUND) {
    return res.status(400).json({
      ...updatedUser,
      error: `Customer with that ID doesn't exist!`,
    });
  }

  return res.status(200).json(updatedUser);
});

/**
 * Calls service function to delete user by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} successful operation confirmation or error message
 */

usersRoute.delete('/:id', authMiddleware, roleValidator, async (req, res) => {
  const userId = +req.params.id;
  const deletedUser = await deleteUser(usersData)(userId);

  if (deletedUser.error === errors.NOT_FOUND) {
    return res.status(400).json({
      ...deletedUser,
      error: `Customer with that ID doesn't exist!`,
    });
  }
  if (deletedUser.error === errors.FORBIDDEN) {
    return res.status(403).json({
      ...deletedUser,
      error: `You are not allowed to delete employees!`,
    });
  }

  return res.sendStatus(204);
});

export default usersRoute;
