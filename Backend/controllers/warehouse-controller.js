import express from 'express';
import validateBody from '../middlewares/validate-body.js';
import createWarehouseValidator from '../validators/create-warehouse-validator.js';
import updateWarehouseValidator from '../validators/update-warehouse-validator.js';
import {
  authMiddleware
} from '../auth/auth-middleware.js';
import roleValidator from '../middlewares/role-validator.js';
import {
  getWarehouseById,
  getAllWarehouses,
  createWarehouse,
  updateWarehouse,
  deleteWarehouse,
} from '../services/warehouse-service.js';
import warehouseData from '../data/warehouse-data.js';
import errors from '../services/errors.js';

const warehouseRouter = express.Router();

/**
 * Calls service function to show a single warehouse by ID
 * @param {req} req request
 * @param {res} res response
 * @return {res} single warehouse data or error message
 */

warehouseRouter.get('/:id', authMiddleware, roleValidator, async (req, res) => {
  const {
    id
  } = req.params;
  const allWarehouses = await getWarehouseById(warehouseData)(id);

  if (allWarehouses.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...allWarehouses,
      error: 'Warehouses not found!'
    });
  }

  return res.status(200).json(allWarehouses);
});


/**
 * Calls service function to show all warehouses
 * @param {req} req request
 * @param {res} res response
 * @return {res} all warehouses data or error message
 */

warehouseRouter.get('/', async (req, res) => {
  const allWarehouses = await getAllWarehouses(warehouseData)();

  if (allWarehouses.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...allWarehouses,
      error: 'Warehouses not found!'
    });
  }

  return res.status(200).json(allWarehouses);
});

/**
 * Calls service function to create a warehouse
 * @param {req} req request
 * @param {res} res response
 * @return {res} created warehouse data or error message
 */

warehouseRouter.post('/', validateBody(createWarehouseValidator), authMiddleware, roleValidator, async (req, res) => {
  const createdWarehouse = await createWarehouse(warehouseData)(req.body);

  if (createdWarehouse.error === errors.OPERATION_FAILED) {
    return res.status(400).json({
      ...createdWarehouse,
      error: 'Warehouse not created!'
    });
  }

  return res.status(201).json(createdWarehouse);
});

/**
 * Calls service function to update a warehouse by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} updated warehouse data or error message
 */

warehouseRouter.put('/:id', validateBody(updateWarehouseValidator), authMiddleware, roleValidator, async (req, res) => {
  const body = req.body;
  const warehouseId = req.params.id;

  const updatedWarehouse = await updateWarehouse(warehouseData)(warehouseId, body);

  if (updatedWarehouse.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...updatedWarehouse,
      error: 'Warehouse with that ID does not exist!'
    });
  }

  return res.status(201).json(updatedWarehouse);
});

/**
 * Calls service function to delete warehouse
 * @param {req} req request
 * @param {res} res response
 * @return {res} successful operation confirmation or error message
 */

warehouseRouter.delete('/:id', authMiddleware, roleValidator, async (req, res) => {
  const warehouseId = req.params.id;

  const deletedWarehouse = await deleteWarehouse(warehouseData)(warehouseId);

  if (deletedWarehouse.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...deletedWarehouse,
      error: 'Warehouse with that ID does not exist!'
    });
  }

  return res.sendStatus(204);
});


export default warehouseRouter;
