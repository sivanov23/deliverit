import express from 'express';
import errors from '../services/errors.js';
import createParcelValidator from '../validators/create-parcel-validator.js';
import validateBody from '../middlewares/validate-body.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import roleValidator from '../middlewares/role-validator.js';
import {
  getAllParcels,
  getParcelsByUserID,
  getAllParcelsByWarehouseId,
  getAllParcelsByCategoryId,
  getAllParcelsByWeight,
  sortParcelsByWeight,
  sortParcelsByArrivalDate,
  sortParcelsByWeightAndArrivalDate,
  createParcel,
  updateParcel,
  deleteParcel,
  getParcelByID,
}
from '../services/parcel-service.js';
import parcelData from '../data/parcel-data.js';


const parcelsRoute = express.Router();

parcelsRoute.use(authMiddleware);
/**
 * Calls service function to show all parcels and allows various filtering and sorting
 * @param {req} req request
 * @param {res} res response
 * @return {res} all parcels data or error message
 */

parcelsRoute.get('/', async (req, res) => {
  const {
    parcelId,
    warehouseId,
    customerId,
    weight,
    category,
    sort,
  } = req.query;
  let parcels;
  if (!warehouseId && !customerId && !weight && !category && !sort && !parcelId) {
    parcels = await getAllParcels(parcelData)();
  } else if (customerId) {
    parcels = await getParcelsByUserID(parcelData)(customerId);
  } else if (parcelId) {
    parcels = await getParcelByID(parcelData)(parcelId);
  } else if (warehouseId) {
    parcels = await getAllParcelsByWarehouseId(parcelData)(warehouseId);
  } else if (category) {
    parcels = await getAllParcelsByCategoryId(parcelData)(category);
  } else if (weight) {
    parcels = await getAllParcelsByWeight(parcelData)(weight);
  } else if (sort) {
    if (sort === 'weight') {
      parcels = await sortParcelsByWeight(parcelData)();
    }
    if (sort === 'arrivalDate') {
      parcels = await sortParcelsByArrivalDate(parcelData)();
    }
    if (sort === 'weight,arrivalDate') {
      parcels = await sortParcelsByWeightAndArrivalDate(parcelData)();
    }
  }
  if (parcels.error === errors.NOT_FOUND) {
    return res.status(404).json({
      ...parcels,
      error: 'Parcels not found!',
    });
  }
  return res.status(200).json(parcels);
});


/**
 * Calls service function to create a parcel
 * @param {req} req request
 * @param {res} res response
 * @return {res} returns the created parcel or error message
 */

parcelsRoute.post('/', roleValidator, async (req, res) => {
  const result = await createParcel(parcelData)(req.body);

  if (result.error === errors.OPERATION_FAILED) {
    return res.status(400).json({
      ...result,
      errors: `Could not create a new parcel!`,
    });
  }

  return res.status(201).json(result);
});


/**
 * Calls service function to update a parcel by id
 * @param {req} req request
 * @param {res} res response
 * @return {res} updated parcel data or error message
 */

parcelsRoute.put('/:id', authMiddleware, roleValidator, validateBody(createParcelValidator), async (req, res) => {
  const result = await updateParcel(parcelData)(req.params.id, req.body);

  if (result.error === errors.NOT_FOUND) {
    return res.status(400).json({
      ...result,
      error: `Parcel not found!`,
    });
  }
  return res.status(200).json(result);
});


/**
 * Calls service function to delete a parcel
 * @param {req} req request
 * @param {res} res response
 * @return {res} returns deleted parcel or error message
 */

parcelsRoute.delete('/:id', authMiddleware, roleValidator, async (req, res) => {
  const result = await deleteParcel(parcelData)(req.params.id);

  if (result.error === errors.NOT_FOUND) {
    return res.status(404).send({
      ...result,
      error: 'Parcel not found!',
    });
  }
  return res.sendStatus(204);
});

export default parcelsRoute;
