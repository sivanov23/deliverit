import pool from './pool.js';

/**
 * Sends SQL request to database to get all countries
 * @return {string} all countries or null if not found
 */

const getAllCountries = async () => {
  const sqlGetAllCountries = `
  SELECT *
  FROM countries`;

  const result = await pool.query(sqlGetAllCountries);

  if (result[0]) {
    return result;
  }

  return null;
};

/**
 * Sends SQL request to database to get all cities
 * @return {string} all cities or null if not found
 */

const getAllCities = async () => {
  const sqlGetAllCities = `
  SELECT *
  FROM cities`;

  const result = await pool.query(sqlGetAllCities);

  if (result[0]) {
    return result;
  }
  return null;
};

/**
 * Sends SQL request to database to get all cities by country id
 * @param {number} countryId id of country
 * @return {string} all cities or null if not found
 */

const getCitiesByCountryId = async (countryId) => {
  const sqlGetCitiesByCountryId = `
  SELECT ci.id as id, ci.name as city_name, co.name as country_name
  FROM cities ci 
  JOIN countries co ON co.id=ci.country_id 
  WHERE co.id = ?`;

  const result = await pool.query(sqlGetCitiesByCountryId, [countryId]);

  if (result[0]) {
    return result;
  }
  return null;
};

/**
 * Sends SQL request to database to get all countries by name
 * @param {string} name name of country
 * @return {string} all cities or null if not found
 */

export const getCountryByName = async (name) => {
  const sqlGetCountryByName =
  `SELECT *
   FROM countries
   WHERE name = ?
   `;

   const country = await pool.query(sqlGetCountryByName, [name]);

   if (country[0]) {
     return country[0];
   }
   return null;
};

/**
 * Sends SQL request to database to create a country
 * @param {string} name name of country
 * @return {number} id of the country or null
 */

export const createCountry = async (name, getCountry) => {
  const sqlGetCountryByName =
    `INSERT 
     INTO countries(name)
     values(?);
   `;

  await pool.query(sqlGetCountryByName, [name]);

  const country = await getCountry(name);

  if (country) {
    return country.id;
  }
  return null;
};

/**
 * Sends SQL request to database to get a city by name
 * @param {string} name name of city
 * @return {string} the city or null
 */

export const getCityByName = async (name) => {
  const sqlGetCityByName =
  `SELECT *
   FROM cities
   WHERE name = ?
   `;

  const city = await pool.query(sqlGetCityByName, [name]);

  if (city[0]) {
    return city[0];
  }
  return null;
};

/**
 * Sends SQL request to database to create a country
 * @param {string} name name of country
 * @param {number} countryId id of country
 * @return {number} id of the city or null
 */

export const createCity = async (name, countryId, getCity) => {
  const sqlGetcityByName =
    `INSERT 
     INTO cities(name, country_id)
     values(?,?);
   `;

  await pool.query(sqlGetcityByName, [name, countryId]);

  const city = await getCity(name);

  if (city) {
    return city.id;
  }
  return null;
};

export default { getAllCountries, getAllCities, getCitiesByCountryId };
