
import pool from './pool.js';
import {
  countries,
  cities,
  users,
  warehouses,
  shipments,
  parcels,
} from './seed-data.js';

(async () => {
  // insert default countries
  for (const country of countries) {
    await pool.query(`
        INSERT INTO countries (id, name)
        VALUES (?, ?)
      `, [country.id, country.name]);
  }
  // insert default cities
  for (const city of cities) {
    await pool.query(`
      INSERT INTO cities (id, country_id, name)
      VALUES (?, ?, ?)
    `, [city.id, city.countryId, city.name]);
  }
  // insert default users
  for (const user of users) {
    await pool.query(`
      INSERT INTO users (id, full_name, password, email, role, country_id, city_id, street)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?)
    `, [user.id, user.fullName, user.password, user.email, user.role, user.countryId, user.cityId, user.street]);
  }
  // insert default warehouses
  for (const warehouse of warehouses) {
    await pool.query(`
      INSERT INTO warehouses (id, country_id, city_id, street, latitude, longitude)
      VALUES (?, ?, ?, ?, ?, ?)
    `, [warehouse.id, warehouse.countryId, warehouse.cityId, warehouse.street, warehouse.latitude, warehouse.longitude]);
  }
  // insert default shipments
  for (const shipment of shipments) {
    await pool.query(`
      INSERT INTO shipments (id, destination_warehouse, origin_warehouse, status)
      VALUES (?, ?, ?, ?)
    `, [shipment.id, shipment.destinationWarehouse, shipment.originWarehouse, shipment.status]);
  }
  // insert default parcels
  for (const parcel of parcels) {
    await pool.query(`
      INSERT INTO parcels (id, weight, category, user_id, shipment_id, deliver_to_address)
      VALUES (?, ?, ?, ?, ?, ?)
    `, [parcel.id, parcel.weight, parcel.category, parcel.userId, parcel.shipmentId, parcel.deliverToAddress]);
  }

  pool.end();
})();
