import pool from './pool.js';

/**
 * Sends SQL request to get a warehouse by id
 * @param {number} id id of warehouse
 * @return {string} warehouse data or null
 */

const selectWarehouseById = async (id) => {
  const sqlSelectWarehouseById = `
    SELECT warehouses.id, co.name as country, ci.name as city, street, latitude, longitude
    FROM warehouses
    JOIN cities ci on city_id = ci.id
    JOIN countries co on warehouses.country_id = co.id
    WHERE warehouses.id = ?
  `;
  const createdWarehouse = (await pool.query(sqlSelectWarehouseById, [id]))[0];

  if (createdWarehouse) {
    return createdWarehouse;
  }

  return null;
};

/**
 * Sends SQL request to get all warehouses
 * @return {string} warehouses data or null
 */

const getAllWarehouses = async () => {
  const sqlGetAllWarehouses = `
  SELECT warehouses.id, co.name as country, ci.name as city, street, latitude, longitude
  FROM warehouses
  JOIN cities ci on city_id = ci.id
  JOIN countries co on warehouses.country_id = co.id`;

  const allWarehouses = await pool.query(sqlGetAllWarehouses);

  if (allWarehouses[0]) {
    return allWarehouses;
  }

  return null;
};

/**
 * Sends SQL request to database to create a new warehouse
 * @param {body} the body of the request
 * @return {string} created warehouse
 */

const createWarehouse = async (body) => {
  // longitude and latitude to be added after Leaflet implementation
  const sqlCreateWareHouse = `
  INSERT
  INTO warehouses(country_id, city_id, street, latitude, longitude)
  VALUES(?,?,?,?,?)`;

  const result = await pool.query(sqlCreateWareHouse, [body.countryId, body.cityId, body.street, body.latitude, body.longitude]);

  return selectWarehouseById(result.insertId);
};

/**
 * Sends SQL request to database to update a warehouse
 * @param {number} warehouseId id of warehouse
 * @param {object} body warehouse data
 * @return {string} updated warehouse
 */

const updateWarehouse = async (warehouseId, body) => {
  const sqlUpdateWarehouse = `
  UPDATE warehouses
  SET country_id = ?, city_id = ?, street = ?, latitude = ?, longitude = ?
  WHERE id = ? 
  `;

  await pool.query(sqlUpdateWarehouse, [body.countryId, body.cityId, body.street, body.latitude, body.longitude, warehouseId]);

  return selectWarehouseById(warehouseId);
};

/**
 * Sends SQL request to database to delete a warehouse
 * @param {number} warehouseId id of warehouse
 * @return {string} deleted warehouse
 */

const deleteWarehouse = async (warehouseId) => {
  const currentWarehouse = await selectWarehouseById(warehouseId);

  const sqlDeleteWarehouse = `
  DELETE
  FROM warehouses
  WHERE id = ? 
  `;

  await pool.query(sqlDeleteWarehouse, [warehouseId]);

  return currentWarehouse;
};

export default {
  selectWarehouseById,
  getAllWarehouses,
  createWarehouse,
  updateWarehouse,
  deleteWarehouse,
};
