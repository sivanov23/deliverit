import pool from './pool.js';

/**
 * Sends SQL request to database to get shipment by id
 * @param {number} shipmentId id of shipment to get
 * @return {string} shipment or null if not found
 */

const getShipmentById = async (shipmentId) => {
  const sqlGetShipmentById = `
    SELECT * 
    FROM shipments
    WHERE id = ?
  `;

  const result = await pool.query(sqlGetShipmentById, [shipmentId]);

  if (result[0]) {
    return result[0];
  }

  return null;
};


/**
 * Sends SQL request to database to get shipments by status id
 * @return {string} shipments or null if not found
 */

 const getShipmentsByStatusId = async () => {
  const sqlGetShipmentsByStatusId = `
    SELECT * 
    FROM shipments
    WHERE status = 1
  `;

  const shipments = await pool.query(sqlGetShipmentsByStatusId);

  if (shipments[0]) {
    return shipments;
  }

  return null;
};

/**
 * Sends SQL request to database to get all shipments
 * @return {string} shipments or null if not found
 */

const getAllShipments = async () => {
  const sqlGetAllShipments = `
  SELECT *
  FROM shipments
  `;
  const shipments = await pool.query(sqlGetAllShipments);

  if (shipments[0]) {
    return shipments;
  }

  return null;
};

/**
 * Sends SQL request to database to get all shipment by user id
 * @param {number} customerId id of user
 * @return {string} shipments or null if not found
 */

const getAllshipmentsByCustomerId = async (customerId) => {
  const sqlGetAllShipments = `
  SELECT s.id, s.origin_warehouse, s.destination_warehouse, s.arrival_date, s.departure_date, s.status, u.id as user_id, u.full_name
  FROM parcels p
  JOIN shipments s on s.id = p.shipment_id
  JOIN users u on u.id = p.user_id
  WHERE u.id = ?;
  `;
  const shipments = await pool.query(sqlGetAllShipments, [customerId]);

  if (shipments[0]) {
    return shipments;
  }

  return null;
};

/**
 * Sends SQL request to database to get all shipment by warehouse id
 * @param {number} warehouseId id of warehouse
 * @return {string} shipments or null if not found
 */

const getAllshipmentsByWarehouseId = async (warehouseId) => {
  const sqlGetAllshipmentsByWarehouseId = `
  SELECT *
  FROM shipments
  WHERE origin_warehouse = ?
  OR
  destination_warehouse = ?
  `;
  const shipments = await pool.query(sqlGetAllshipmentsByWarehouseId, [warehouseId, warehouseId]);

  if (shipments[0]) {
    return shipments;
  }

  return null;
};

/**
 * Sends SQL request to database to create a shipment
 * @param {Date} arrivalDate date of arrival
 * @param {Date} departureDate date of departure
 * @param {number} destinationWarehouseId id of destination warehouse
 * @param {number} originWarehouseId id of origin warehouse
 * @param {number} status status of shipment
 * @return {string} shipment data
 */

const createShipment = async (originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate) => {
  const sqlCreateShipment = `
    INSERT 
    INTO shipments(origin_warehouse, destination_warehouse, status, arrival_date, departure_date)
    values(?,?,?,?,?)
  `;
  const result = await pool.query(sqlCreateShipment, [originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate]);

  return getShipmentById(result.insertId);
};
/**
 * Sends SQL request to database to update a shipment by id
 * @param {object} body update data
 * @param {number} shipmentId id of shipment
 * @return {string} shipment data
 */

const updateShipment = async (shipmentId, body) => {
  const sqlUpdateShipment = `
    UPDATE shipments
    SET origin_warehouse = ?, destination_warehouse = ?, departure_date = ?, arrival_date = ?, status = ?
    WHERE id = ?
  `;

  await pool.query(sqlUpdateShipment, [body.originWarehouseId, body.destinationWarehouseId, body.departureDate ? new Date(body.departureDate) : null, body.arrivalDate ? new Date(body.arrivalDate) : null, body.status, shipmentId]);

  const result = getShipmentById(shipmentId);

  return result;
};

/**
 * Sends SQL request to database to delete a shipment by id
 * @param {number} shipmentId id of shipment
 * @return {string} shipment data
 */

const deleteShipment = async (shipmentId) => {
  const result = getShipmentById(shipmentId);

  const sqldeleteShipment = `
    DELETE
    FROM shipments
    WHERE id = ?
  `;

  await pool.query(sqldeleteShipment, [shipmentId]);

  return result;
};


export default {
  getShipmentById,
  getAllShipments,
  getAllshipmentsByWarehouseId,
  getAllshipmentsByCustomerId,
  createShipment,
  updateShipment,
  deleteShipment,
  getShipmentsByStatusId,
};
