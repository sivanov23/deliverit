/* eslint-disable linebreak-style */
/* eslint-disable max-len */
import pool from './pool.js';

const userExists = async (email) => {
  return !!(await getSingleUserByEmail(email));
};


/**
 * Sends SQL request to database to get user by id
 * @param {number} userId user id of user to get
 * @return {string} user or null if not found
 */

const getUserById = async (userId) => {
  const sql = `
    SELECT u.id, u.full_name, u.email, u.role, u.country_id, u.city_id, u.street
    FROM users u
    WHERE id = ?;
  `;

  const user = await pool.query(sql, [userId]);

  if (user[0]) {
    return user[0];
  }

  return null;
};

/**
 * Sends SQL request to database to get all users
 * @return {string} users or null if not found
 */

const getAllUsers = async () => {
  return await pool.query(`
    SELECT u.id, u.full_name, u.email, u.role, u.country_id, u.city_id, u.street, co.name as country,ci.name as city
    FROM users u
    JOIN countries co ON u.country_id = co.id
    JOIN cities ci ON u.city_id = ci.id;
  `);
};

/**
 * Sends SQL request to database to get user by email
 * @param {string} email email of user to get
 * @return {string} user or null if not found
 */

const getSingleUserByEmail = async (email) => {
  const res = await pool.query(`
    SELECT *
    FROM users u 
    WHERE u.email = ?`, [email]);

  if (res[0]) {
    return res[0];
  }
  return null;
};

/**
 * Sends SQL request to database to search for all users by email
 * @param {string} email email of user to search
 * @return {string} user or null if not found
 */

const getUsersByEmail = async (email) => {
  const enhancedEmail = `%${email}%`;
  const res = await pool.query(`
    SELECT u.id, u.full_name, u.email, u.role, u.country_id, u.city_id, u.street
    FROM users u 
    WHERE u.email LIKE ?`, [enhancedEmail]);

  if (res[0]) {
    return res;
  }
  return null;
};

/**
 * Sends SQL request to database to get user by name
 * @param {string} name name of user to get
 * @return {string} user or null if not found
 */

const getUsersByFullName = async (name) => {
  const enhancedName = `%${name}%`;
  const res = await pool.query(`
    SELECT u.id, u.full_name, u.email, u.role, u.country_id, u.city_id, u.street
    FROM users u 
    WHERE u.full_name LIKE ?`, [enhancedName]);

  if (res[0]) {
    return res;
  }
  return null;
};

/**
 * Sends SQL request to database to get incoming parcels by userId
 * @param {string} userId Id of user to get
 * @return {string} parcels or null if not found
 */

const getIncomingParcels = async (userId) => {
  const sqlGetIncomingParcels = `
    SELECT p.id as id, weight, category, user_id, shipment_id, deliver_to_address, status
    FROM parcels p
    JOIN shipments s on p.shipment_id = s.id
    WHERE p.user_id = ?
    AND NOT s.status = 3;
  `;

  const incomingParcels = await pool.query(sqlGetIncomingParcels, [userId]);

  if (incomingParcels[0]) {
    return incomingParcels;
  }
  return null;
};

/**
 * Sends SQL request to database to search for a user by email and name
 * @param {string} searchData search data of user to find
 * @return {string} user or null if not found
 */

const searchUser = async (searchData) => {
  const enhancedSearchData = `%${searchData}%`;

  const sqlSearchData = `
    SELECT u.id, u.full_name, u.email, u.country_id, u.city_id, u.street
    FROM users u
    WHERE role = 1 AND email LIKE ?  
    OR role = 1 AND full_name LIKE ?;
  `;
  const searchResults = await pool.query(sqlSearchData, [enhancedSearchData, enhancedSearchData]);

  if (searchResults[0]) {
    return searchResults;
  }
  return null;
};

/**
 * Sends SQL request to database to create a new user
 * @param {object} user user information
 * @param {string} hashedPassword hashed password of new user
 * @return {string} created user
 */

const createUser = async (user, hashPassword) => {
  const sql = `
    insert into Users (email ,full_name, password, country_id, city_id, street)
    values (?, ?, ?, ?, ?, ?)
  `;

  await pool.query(sql, [user.email, user.fullName, hashPassword, user.countryId, user.cityId, user.street]);

  return getUsersByEmail(user.email);
};


/**
 * Sends SQL request to database to update an user
 * @param {number} id user id
 * @param {object} user user information
 * @return {string} updated user
 */

const updateUser = async (id, user) => {
  const sql = `
    UPDATE users
    SET email = ?, full_name = ?, country_id = ?, city_id = ?, street = ?
    WHERE id = ?;
  `;

  await pool.query(sql, [user.email, user.fullName, user.countryId, user.cityId, user.street, id]);

  return getUserById(id);
};


/**
 * Sends SQL request to database to delete an user
 * @param {number} id user id
 * @return {string} deleted user
 */

const deleteUser = async (id) => {
  const user = await getUserById(id);

  const sql = `
    DELETE
    FROM users
    WHERE id = ?;
  `;

  await pool.query(sql, [id]);

  return user;
};

export default {
  userExists,
  getUserById,
  getAllUsers,
  getSingleUserByEmail,
  getUsersByEmail,
  getUsersByFullName,
  getIncomingParcels,
  searchUser,
  createUser,
  updateUser,
  deleteUser,
};
