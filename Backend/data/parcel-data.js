import pool from './pool.js';

const parcelExists = async (id) => {
  return !!(await getSingleParcelById(id));
};

/**
 * Sends SQL request to database to get all parcels
 * @return {string} all parcels or null if not found
 */

const getAllParcels = async () => {
  const allParcels = await pool.query(`
  SELECT p.id, p.weight, p.category, p.user_id,p.shipment_id, s.status,u.full_name
  FROM parcels p
  JOIN users u ON p.user_id = u.id
  JOIN shipments s ON p.shipment_id = s.id
  `);

  if (allParcels[0]) {
    return allParcels;
  }

  return null;
};

/**
 * Sends SQL request to database to get parcel by id
 * @param {number} id id of parcel
 * @return {string} parcel or null if not found
 */

const getSingleParcelById = async (id) => {
  const res = await pool.query(`
  SELECT * 
  FROM parcels p 
  WHERE p.id = ?`, [id]);

  if (res[0]) {
    return res[0];
  }

  return null;
};

/**
 * Sends SQL request to database to get parcel by user id
 * @param {number} userId id of user
 * @return {string} parcels or null if not found
 */

const getParcelsByUserId = async (userId) => {
  const res = await pool.query(`
  SELECT p.id, p.weight, p.category, p.user_id,p.shipment_id, s.status
  FROM parcels p 
  JOIN shipments s ON p.shipment_id = s.id
  WHERE p.user_id = ?`, [userId]);

  if (res[0]) {
    return res;
  }

  return null;
};

/**
 * Sends SQL request to database to get parcel by warehouse id
 * @param {number} warehouseId id of warehouse
 * @return {string} parcels or null if not found
 */

const getParcelsByWarehouseId = async (warehouseId) => {
  const sql = `
  SELECT p.id, p.weight, p.category,p.user_id, p.shipment_id, p.deliver_to_address
  FROM parcels p
  JOIN shipments s 
  ON p.shipment_id = s.id
  WHERE s.origin_warehouse = ? AND s.status = 1;
  `;
  const parcels = await pool.query(sql, [warehouseId, warehouseId]);

  if (parcels[0]) {
    return parcels;
  }

  return null;
};

/**
 * Sends SQL request to database to get parcel by category id
 * @param {number} category id of category
 * @return {string} parcels or null if not found
 */

const getParcelsByCategoryId = async (category) => {
  const res = await pool.query(`
  SELECT *
  FROM parcels p
  WHERE p.category = ?`, [category]);

  if (res[0]) {
    return res;
  }

  return null;
};

/**
 * Sends SQL request to database to get parcels by weight
 * @param {number} weight weight to search parcels by
 * @return {string} parcels or null if not found
 */

const getParcelsByWeight = async (weight) => {
  const res = await pool.query(`
  SELECT *
  FROM parcels
  WHERE weight = ?`, [weight]);

  if (res[0]) {
    return res;
  }

  return null;
};

/**
 * Sends SQL request to database to sort parcels by weight
 * @return {string} sorted parcels or null if not found
 */

const sortParcelsByWeight = async () => {
  const parcelsSortedByWeight = await pool.query(`
    SELECT * 
    FROM parcels
    ORDER BY weight desc;
  `);

  if (parcelsSortedByWeight[0]) {
    return parcelsSortedByWeight;
  }
  return null;
};

/**
 * Sends SQL request to database to sort parcels by arrival date
 * @return {string} sorted parcels or null if not found
 */

const sortParcelsByArrivalDate = async () => {
  const parcelsSortedByArrivalDate = await pool.query(`
    SELECT p.id, p.weight, p.category,p.user_id, p.shipment_id, p.deliver_to_address,s.arrival_date
    FROM parcels p
    JOIN shipments s
    ON p.shipment_id = s.id
    ORDER BY s.arrival_date desc;
  `);

  if (parcelsSortedByArrivalDate[0]) {
    return parcelsSortedByArrivalDate;
  }

  return null;
};

/**
 * Sends SQL request to database to sort parcels by arrival date and weight
 * @return {string} sorted parcels or null if not found
 */

const sortParcelsByWeightAndArrivalDate = async () => {
  const parcelsSortedByWeightAndArrivalDate = await pool.query(`
    SELECT p.id, p.weight, p.category,p.user_id, p.shipment_id, p.deliver_to_address,s.arrival_date
    FROM parcels p
    JOIN shipments s
    ON p.shipment_id = s.id
    ORDER BY p.weight DESC, s.arrival_date DESC;
  `);

  if (parcelsSortedByWeightAndArrivalDate[0]) {
    return parcelsSortedByWeightAndArrivalDate;
  }

  return null;
};

/**
 * Sends SQL request to database to get all parcels by customer id with status that is not 3
 * @param {number} customerId id of user
 * @return {string} returns all shipments or null
 */

const getAllIncompletedParcelsByCustomerId = async (customerId) => {
  const sqlGetAllShipments = `
  SELECT p.id as id,concat(w.street, ', ', t.name, ', ', c.name) as parcel_location, p.weight, p.category, p.shipment_id, s.status
  FROM parcels p
  JOIN shipments s on s.id = p.shipment_id
  JOIN warehouses w on w.id = s.origin_warehouse
  JOIN cities t on t.id = w.city_id
  JOIN countries c on c.id = w.country_id
  WHERE NOT s.status = 3 AND p.user_id = ? ;
  `;
  const shipments = await pool.query(sqlGetAllShipments, [customerId]);

  if (shipments[0]) {
    return shipments;
  }

  return null;
};

/**
 * Sends SQL request to database to get all parcels by customer id with status that is 3
 * @param {number} customerId id of user
 * @return {string} returns all shipments or null
 */

const getAllCompletedParcelsByCustomerId = async (customerId) => {
  const sqlGetAllShipments = `
   SELECT p.id as id,concat(w.street, ', ', t.name, ', ', c.name) as parcel_location, p.weight, p.category, p.shipment_id, s.status, p.user_id
   FROM parcels p
   JOIN shipments s on s.id = p.shipment_id
   JOIN warehouses w on w.id = s.destination_warehouse
   JOIN cities t on t.id = w.city_id
   JOIN countries c on c.id = w.country_id
   WHERE s.status = 3 AND p.user_id = ?;
  `;
  const shipments = await pool.query(sqlGetAllShipments, [customerId]);

  if (shipments[0]) {
    return shipments;
  }

  return null;
};

/**
 * Sends SQL request to database to create a new parcel
 * @param {object} parcel parcel data
 * @return {string} created parcel
 */

const createParcel = async (parcel) => {
  const sql = `
    INSERT 
    INTO parcels (weight, category, user_id, shipment_id, deliver_to_address)
    VALUES (?, ?, ?, ?, ?)
  `;
  const result = await pool.query(sql, [parcel.weight, parcel.category, parcel.userId, parcel.shipmentId, parcel.deliverToAddress]);

  return getSingleParcelById(result.insertId);
};

/**
 * Sends SQL request to database to update a parcel
 * @param {object} body parcel data
 * @param {number} id id of parcel
 * @return {string} updated parcel
 */

const updateParcel = async (id, body) => {
  const sql = `
  UPDATE parcels 
  SET weight = ?, category = ?, user_id = ?, shipment_id = ?, deliver_to_address = ?
  WHERE id = ?;
  `;

  await pool.query(sql, [body.weight, body.category, body.userId, body.shipmentId, body.deliverToAddress, id]);

  return getSingleParcelById(id);
};

/**
 * Sends SQL request to database to delete a parcel
 * @param {object} parcel parcel data
 * @return {string} deleted parcel
 */

const deleteParcel = async (parcel) => {
  const result = getSingleParcelById(parcel.id);

  const sql = `
      DELETE FROM parcels 
      WHERE id = ?
  `;

  await pool.query(sql, [parcel.id]);

  return result;
};

export default {
  parcelExists,
  getAllParcels,
  getSingleParcelById,
  getParcelsByUserId,
  getParcelsByWarehouseId,
  getParcelsByCategoryId,
  getParcelsByWeight,
  sortParcelsByWeight,
  sortParcelsByArrivalDate,
  sortParcelsByWeightAndArrivalDate,
  getAllIncompletedParcelsByCustomerId,
  getAllCompletedParcelsByCustomerId,
  createParcel,
  updateParcel,
  deleteParcel,
};
