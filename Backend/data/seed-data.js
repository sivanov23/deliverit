import bcrypt from 'bcrypt';
import userRole from '../common/user-role.js';
import shipmentStatus from '../common/shipment-status.js';

// create a default password
const seededUserPssword = 'Adminpass';
// hash the password in 10 rounds of calculations
const hashPassword = async () => {
  return await bcrypt.hash(seededUserPssword, 10);
};
const hashedUserPssword = await hashPassword();

export const countries = [
  {
    id: 1,
    name: 'Bulgaria',
  },
  {
    id: 2,
    name: 'England',
  },
  {
    id: 3,
    name: 'Belgium',
  },
  {
    id: 4,
    name: 'Tajikistan',
  },
  {
    id: 5,
    name: 'Greece',
  },
];

export const cities = [
  {
    id: 1,
    countryId: 1,
    name: 'Pleven',
  },
  {
    id: 2,
    countryId: 1,
    name: 'Sofia',
  },
  {
    id: 3,
    countryId: 2,
    name: 'London',
  },
  {
    id: 4,
    countryId: 2,
    name: 'Liverpool',
  },
  {
    id: 5,
    countryId: 3,
    name: 'Antwerp',
  },
  {
    id: 6,
    countryId: 3,
    name: 'Brussels',
  },
  {
    id: 7,
    countryId: 4,
    name: 'Dushanbe',
  },
  {
    id: 8,
    countryId: 4,
    name: 'Khujand',
  },
  {
    id: 9,
    countryId: 5,
    name: 'Athens',
  },
  {
    id: 10,
    countryId: 5,
    name: 'Santorini',
  },
];

export const users = [
  {
    id: 1,
    fullName: 'Example Employee',
    email: 'sample.employee@abv.bg',
    password: hashedUserPssword,
    role: userRole.EMPLOYEE,
    countryId: 1,
    cityId: 1,
    street: '1 Street Name',
  },
  {
    id: 2,
    fullName: 'Example Customer',
    email: 'sample.customer@gmail.com',
    password: hashedUserPssword,
    role: userRole.CUSTOMER,
    countryId: 2,
    cityId: 3,
    street: '2 Street Name',
  },
  {
    id: 3,
    fullName: 'Romelu Lukaku',
    email: 'bigRom9@mail.be',
    password: hashedUserPssword,
    role: userRole.CUSTOMER,
    countryId: 3,
    cityId: 5,
    street: '60 Wiertzstraat',
  },
  {
    id: 4,
    fullName: 'Usain Bolt',
    email: 'ligntning.quick@olympic.mail',
    password: hashedUserPssword,
    role: userRole.CUSTOMER,
    countryId: 1,
    cityId: 1,
    street: '38 Panega Street',
  },
  {
    id: 5,
    fullName: 'Giorgos Mazonakis',
    email: '2Gucci4Ema@shanomail.gr',
    password: hashedUserPssword,
    role: userRole.CUSTOMER,
    countryId: 5,
    cityId: 9,
    street: 'Tzireon 3',
  },
  {
    id: 6,
    fullName: 'Mitachi',
    email: 'Mitachi@abv.bg',
    password: hashedUserPssword,
    role: userRole.EMPLOYEE,
    countryId: 1,
    cityId: 2,
    street: '17 Vitoshka Street',
  },
];

export const warehouses = [
  {
    id: 1,
    countryId: 1,
    cityId: 1,
    street: '74 Vasil Levski',
    latitude: 51.4819543740,
    longitude: -0.1909226357,
  },
  {
    id: 2,
    countryId: 1,
    cityId: 2,
    street: '1 Purva',
    latitude: 51.5007793689,
    longitude: -0.1246284531,
  },
  {
    id: 3,
    countryId: 2,
    cityId: 3,
    street: '101 Fulham Road',
    latitude: 51.2054493633,
    longitude: -0.1909226357,
  },
  {
    id: 4,
    countryId: 2,
    cityId: 4,
    street: 'Anfield Road',
    latitude: 50.8976312952,
    longitude: -0.1246284531,
  },
  {
    id: 5,
    countryId: 3,
    cityId: 5,
    street: '47 Boulevard d\'Honor',
    latitude: 51.2054493633,
    longitude: 4.3930340053,
  },
  {
    id: 6,
    countryId: 3,
    cityId: 6,
    street: '12 Lavouasier Square',
    latitude: 50.8976312952,
    longitude: 4.3410558317,
  },
  {
    id: 7,
    countryId: 4,
    cityId: 7,
    street: '28 I dont know any streets here bro',
    latitude: 38.5868157412,
    longitude: 68.7781385453,
  },
  {
    id: 8,
    countryId: 4,
    cityId: 8,
    street: '42 Somewhere Street',
    latitude: 40.3055074498,
    longitude: 69.6145656934,
  },
  {
    id: 9,
    countryId: 5,
    cityId: 9,
    street: '11 Aristotelous Boulevard',
    latitude: 37.9716976339,
    longitude: 23.7267165977,
  },
  {
    id: 10,
    countryId: 5,
    cityId: 10,
    street: '12 Zeus Street',
    latitude: 36.4616971768,
    longitude: 25.3779078688,
  },
];

export const shipments = [
  {
    id: 1,
    originWarehouse: 1,
    destinationWarehouse: 2,
    status: shipmentStatus.PREAPARING,
  },
  {
    id: 2,
    originWarehouse: 3,
    destinationWarehouse: 4,
    status: shipmentStatus.PREAPARING,
  },
  {
    id: 3,
    originWarehouse: 5,
    destinationWarehouse: 6,
    status: shipmentStatus.ON_THE_WAY,
  },
  {
    id: 4,
    originWarehouse: 7,
    destinationWarehouse: 8,
    status: shipmentStatus.ON_THE_WAY,
  },
  {
    id: 5,
    originWarehouse: 9,
    destinationWarehouse: 10,
    status: shipmentStatus.COMPLETED,
  },
  {
    id: 6,
    originWarehouse: 1,
    destinationWarehouse: 10,
    status: shipmentStatus.ON_THE_WAY,
  },
  {
    id: 7,
    originWarehouse: 10,
    destinationWarehouse: 1,
    status: shipmentStatus.COMPLETED,
  },
];

export const parcels = [
  {
    id: 1,
    weight: 10.05,
    category: 1,
    userId: 2,
    shipmentId: 1,
    deliverToAddress: 0,
  },
  {
    id: 2,
    weight: 5,
    category: 2,
    userId: 2,
    shipmentId: 2,
    deliverToAddress: 1,
  },
  {
    id: 3,
    weight: 10,
    category: 3,
    userId: 3,
    shipmentId: 2,
    deliverToAddress: 0,
  },
  {
    id: 4,
    weight: 5,
    category: 4,
    userId: 2,
    shipmentId: 3,
    deliverToAddress: 1,
  },
  {
    id: 5,
    weight: 10.75,
    category: 7,
    userId: 4,
    shipmentId: 1,
    deliverToAddress: 0,
  },
  {
    id: 6,
    weight: 12,
    category: 1,
    userId: 4,
    shipmentId: 1,
    deliverToAddress: 0,
  },
  {
    id: 7,
    weight: 17.534,
    category: 1,
    userId: 3,
    shipmentId: 1,
    deliverToAddress: 0,
  },
];

