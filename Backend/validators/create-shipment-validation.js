export default {
  originWarehouseId: (value) => {
    if (!value) {
      return 'originWarehouseId is required!';
    }
    if (typeof +value !== 'number' || +value < 1) {
      return 'originWarehouseId should be a positive integer';
    }
    return null;
  },
  destinationWarehouseId: (value) => {
    if (!value) {
      return 'destinationWarehouseId is required!';
    }
    if (typeof +value !== 'number' || +value < 1) {
      return 'destinationWarehouseId should be a positive integer';
    }
    return null;
  },
  status: (value) => {
    if (!value) {
      return 'status is required';
    }
    if (typeof +value !== 'number' || +value < 1 || +value > 3) {
      return 'Invalid street name and number';
    }
    return null;
  },
  arrivalDate: (value) => {
    if (value === undefined) {
      return 'arrivalDate is required';
    }
    const date = new Date(value);
    if (value !== null && Number.isNaN(date.getFullYear())) {
      return 'Invalid date format';
    }
    return null;
  },
  departureDate: (value) => {
    if (value === undefined) {
      return 'arrivalDate is required';
    }
    const date = new Date(value);
    if (value !== null && Number.isNaN(date.getFullYear())) {
      return 'Invalid date format';
    }
    return null;
  },
};
