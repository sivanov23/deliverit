export default {
  email: (value) => {
    if (typeof value !== 'string') {
      return 'email should be a string!';
    }
    if (value.length < 8) {
      return 'email should be at least 8 symbols long!';
    }
    const regexEmailValidator = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (!value.match(regexEmailValidator)) {
      return 'Invalid email format!';
    }
    return null;
  },
  street: (value) => {
    if (typeof value !== 'string') {
      return 'street must be a string!';
    }
    if (value.length < 5) {
      return 'email must be at least 5 symbols long!';
    }
    return null;
  },
  countryId: (value) => {
    if (typeof +value !== 'number') {
      return 'countryId must be a number!';
    }
    if (+value < 0) {
      return 'countryId must be a positive integer!';
    }
    return null;
  },
  cityId: (value) => {
    if (typeof +value !== 'number') {
      return 'cityId must be a number!';
    }
    if (+value < 0) {
      return 'cityId must be a positive integer!';
    }
    return null;
  },
};
