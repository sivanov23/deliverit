export default {
  weight: (value) => {
    if (!value) {
      return 'weight is required!';
    }
    if (typeof +value !== 'number' || +value < 1) {
      return 'weight should be a positive integer';
    }
    return null;
  },
  category: (value) => {
    if (!value) {
      return 'category is required!';
    }
    if (typeof +value !== 'number' || +value < 1) {
      return 'category should be a positive integer!';
    }
    return null;
  },
  userId: (value) => {
    if (!value) {
      return 'userId is required!';
    }
    if (typeof +value !== 'number' || +value < 1) {
      return 'userId should be a positive integer!';
    }
    return null;
  },
  shipmentId: (value) => {
    if (!value) {
      return 'shipmentId is required!';
    }
    if (typeof +value !== 'number' || +value < 1) {
      return 'shipmentId should be a positive integer!';
    }
    return null;
  },
  deliverToAddress: (value) => {
    if (!value) {
      return 'deliverToAdress value is required!';
    }
    if (typeof +value !== 'number' || +value < 0 || +value > 1) {
      return 'deliverToAddress should be either 0 or 1!';
    }
    return null;
  },
};

