export default {
  country: (value) => {
    if (!value) {
      return 'country is required!';
    }
    if (typeof value !== 'string' || value.length < 2) {
      return 'country should be a string and at least 2 characters long!';
    }
    return null;
  },
  city: (value) => {
    if (!value) {
      return 'city is required!';
    }
    if (typeof value !== 'string') {
      return 'city should be a string!';
    }
    return null;
  },
  street: (value) => {
    if (!value) {
      return 'street is required!';
    }
    if (typeof value !== 'string' || value.length < 5) {
      return 'Invalid street name and number!';
    }
    return null;
  },
  latitude: (value) => {
    if (!value) {
      return 'latitude is required!';
    }
    if (typeof +value !== 'number' || value < -90 || value > 90) {
      return 'Invalid latitude!';
    }
    return null;
  },
  longitude: (value) => {
    if (!value) {
      return 'longitude is required!';
    }
    if (typeof +value !== 'number' || value < -180 || value > 180) {
      return 'Invalid longitude!';
    }
    return null;
  },

};
