/* eslint-disable max-len */
/* eslint-disable linebreak-style */
import errors from './errors.js';

/**
 * Calls data functions to validate the parcel's information to show all parcels
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @return {Object} { error if exists, parcel data if no error }
 */


export const getAllParcels = (parcelData) => async () => {
  const allParcels = await parcelData.getAllParcels();
  if (allParcels === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allParcels,
  };
};

/**
 * Calls data functions to validate the parcel's information to show a parcel by id
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @param {number} id id of parcel
 * @return {Object} { error if exists, parcel data if no error }
 */

export const getParcelByID = (parcelData) => async (id) => {
  const parcel = await parcelData.getSingleParcelById(id);

  if (parcel === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: parcel,
  };
};

/**
 * Calls data functions to validate the parcel's information to show a parcel by user id and by status
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @param {number} id id of user
 * @return {Object} { error if exists, parcel data if no error }
 */

export const getParcelsByUserID = (parcelData) => async (id) => {
  const completedParcels = await parcelData.getAllCompletedParcelsByCustomerId(id);
  const travellingParcels = await parcelData.getAllIncompletedParcelsByCustomerId(id);

  const returnEmptyArrayIfNull = (data) => {
    if (!data) {
      return [];
    }
    return data;
  };

  if (!completedParcels && !travellingParcels) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: {
      completedParcels: returnEmptyArrayIfNull(completedParcels),
      travellingParcels: returnEmptyArrayIfNull(travellingParcels),
    },
  };
};

/**
 * Calls data functions to validate the parcel's information to show all parcels by warehouse id
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @param {number} warehouseId id of warehouse
 * @return {Object} { error if exists, parcel data if no error }
 */

export const getAllParcelsByWarehouseId = (parcelData) => async (warehouseId) => {
  const parcelsByWarehouseId = await parcelData.getParcelsByWarehouseId(warehouseId);
  if (!parcelsByWarehouseId) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: parcelsByWarehouseId,
  };
};

/**
 * Calls data functions to validate the parcel's information to show all parcels by category id
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @param {number} category id of category
 * @return {Object} { error if exists, parcel data if no error }
 */

export const getAllParcelsByCategoryId = (parcelData) => async (category) => {
  const parcelsByCategoryId = await parcelData.getParcelsByCategoryId(category);
  if (!parcelsByCategoryId) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: parcelsByCategoryId,
  };
};

/**
 * Calls data functions to validate the parcel's information to show all parcels by weight
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @param {number} weight weight value
 * @return {Object} { error if exists, parcel data if no error }
 */

export const getAllParcelsByWeight = (parcelData) => async (weight) => {
  const parcelsByWeight = await parcelData.getParcelsByWeight(weight);
  if (!parcelsByWeight) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: parcelsByWeight,
  };
};

/**
 * Calls data functions to validate the parcel's information to sort all parcels by weight
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @return {Object} { error if exists, parcel data if no error }
 */

export const sortParcelsByWeight = (parcelData) => async () => {
  const parcelsByWeightSorted = await parcelData.sortParcelsByWeight();
  if (!parcelsByWeightSorted) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: parcelsByWeightSorted,
  };
};

/**
 * Calls data functions to validate the parcel's information to sort all parcels by arrival date
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @return {Object} { error if exists, parcel data if no error }
 */

export const sortParcelsByArrivalDate = (parcelData) => async () => {
  const parcelsByArrivalDataSorted = await parcelData.sortParcelsByArrivalDate();
  if (!parcelsByArrivalDataSorted) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: parcelsByArrivalDataSorted,
  };
};

/**
 * Calls data functions to validate the parcel's information to sort all parcels by arrival date and weight
 *
 * @param {Object} parcelData collection of functions that send queries to the database
 * @return {Object} { error if exists, parcel data if no error }
 */

export const sortParcelsByWeightAndArrivalDate = (parcelData) => async () => {
  const result = await parcelData.sortParcelsByWeightAndArrivalDate();
  if (!result) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: result,
  };
};

/**
 * Calls data functions to validate the parcel's information to create a parcel
 * @param {Object} parcel parcel data
 * @param {Object} parcelData collection of functions that send queries to the database
 * @return {Object} { error if exists, parcel data if no error }
 */

export const createParcel = (parcelData) => async (parcel) => {
  const createdParcel = await parcelData.createParcel(parcel);

  if (!createdParcel) {
    return {
      error: errors.OPERATION_FAILED,
      data: null,
    };
  }

  return {
    error: null,
    data: createdParcel,
  };
};

/**
 * Calls data functions to validate the parcel's information to update a parcel
 * @param {Object} body parcel data
 * @param {number} id parcel id
 * @param {Object} parcelData collection of functions that send queries to the database
 * @return {Object} { error if exists, parcel data if no error }
 */

export const updateParcel = (parcelData) => async (id, body) => {
  const parcelToUpdate = await parcelData.getSingleParcelById(id);
  if (!parcelToUpdate) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  const updatedParcel = await parcelData.updateParcel(id, body);

  return {
    error: null,
    data: updatedParcel,
  };
};

/**
 * Calls data functions to validate the parcel's information to delete a parcel
 * @param {number} id parcel id
 * @param {Object} parcelData collection of functions that send queries to the database
 * @return {Object} { error if exists, parcel data if no error }
 */

export const deleteParcel = (parcelData) => async (id) => {
  const parcelToDelete = await parcelData.getSingleParcelById(id);
  if (!parcelToDelete) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  await parcelData.deleteParcel(parcelToDelete);

  return {
    error: null,
    data: parcelToDelete,
  };
};


export default {
  getAllParcels,
  getParcelByID,
  getParcelsByUserID,
  getAllParcelsByWarehouseId,
  getAllParcelsByCategoryId,
  getAllParcelsByWeight,
  sortParcelsByWeight,
  sortParcelsByArrivalDate,
  sortParcelsByWeightAndArrivalDate,
  createParcel,
  updateParcel,
  deleteParcel,
};
