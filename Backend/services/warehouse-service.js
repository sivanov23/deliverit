import errors from './errors.js';
import { getCityByName, createCity, getCountryByName, createCountry } from '../data/location-data.js';

/**
 * Calls data functions to validate the warehouse's information to show a single warehouse by id
 * @param {Object} warehouseData collection of functions that send queries to the database
 * @return {Object} { error if exists, warehouse data if no error }
 */

export const getWarehouseById = (warehouseData) => async (id) => {
  const singleWarehouse = await warehouseData.selectWarehouseById(id);
  if (singleWarehouse) {
    return {
      data: singleWarehouse,
      error: null,
    };
  }
  return {
    data: null,
    error: errors.NOT_FOUND,
  };
};

/**
 * Calls data functions to validate the warehouse's information to show all warehouses
 * @param {Object} warehouseData collection of functions that send queries to the database
 * @return {Object} { error if exists, warehouse data if no error }
 */

export const getAllWarehouses = (warehouseData) => async () => {
  const allWarehouses = await warehouseData.getAllWarehouses();
  if (allWarehouses) {
    return {
      data: allWarehouses,
      error: null,
    };
  }
    return {
      data: null,
      error: errors.NOT_FOUND,
    };
};

/**
 * Calls data functions to validate the warehouse's information to create a warehouse
 * @param {Object} warehouseData collection of functions that send queries to the database
 * @param {number} body the request body params combined
 * @return {Object} { error if exists, warehouse data if no error }
 */

export const createWarehouse = (warehouseData) => async (body) => {
  let countryId;
  let cityId;

  const country = await getCountryByName(body.country);
  if (!country) {
    countryId = await createCountry(body.country, getCountryByName);
  } else {
    countryId = country.id;
  }

  const city = await getCityByName(body.city);
  if (!city) {
    cityId = await createCity(body.city, countryId, getCityByName);
  } else {
    cityId = city.id;
  }

  const createdWarehouse = await warehouseData.createWarehouse({ ...body, cityId, countryId });
  if (createdWarehouse) {
    return {
      data: createdWarehouse,
      error: null,
    };
  }

  return {
    data: null,
    error: errors.NOT_IMPLEMENTED,
  };
};

/**
 * Calls data functions to validate the warehouse's information to update a warehouse
 * @param {Object} warehouseData collection of functions that send queries to the database
 * @param {number} warehouseId id of warehouse
 * @param {Object} the body of the request
 * @return {Object} { error if exists, warehouse data if no error }
 */

export const updateWarehouse = (warehouseData) => async (warehouseId, body) => {
  let countryId;
  let cityId;

  const country = await getCountryByName(body.country);
  if (!country) {
    countryId = await createCountry(body.country, getCountryByName);
  } else {
    countryId = country.id;
  }

  const city = await getCityByName(body.city);
  if (!city) {
    cityId = await createCity(body.city, countryId, getCityByName);
  } else {
    cityId = city.id;
  }
  const updatedWarehouse = await warehouseData.updateWarehouse(warehouseId, { ...body, cityId, countryId });
  if (updatedWarehouse) {
    return {
      data: updatedWarehouse,
      error: null,
    };
  }

  return {
    data: null,
    error: errors.NOT_FOUND,
  };
};

/**
 * Calls data functions to validate the warehouse's information to delete a warehouse
 * @param {Object} warehouseData collection of functions that send queries to the database
 * @param {number} warehouseId id of warehouse
 * @return {Object} { error if exists, warehouse data if no error }
 */

export const deleteWarehouse = (warehouseData) => async (warehouseId) => {
  const deletedWarehouse = await warehouseData.deleteWarehouse(warehouseId);
  if (deletedWarehouse) {
    return {
      data: deletedWarehouse,
      error: null,
    };
  }

  return {
    data: null,
    error: errors.NOT_FOUND,
  };
};


export default {
  getWarehouseById,
  getAllWarehouses,
  createWarehouse,
  updateWarehouse,
  deleteWarehouse,
};
