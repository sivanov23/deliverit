/* eslint-disable max-len */
/* eslint-disable linebreak-style */
import errors from './errors.js';
import bcrypt from 'bcrypt';
import userRole from '../common/user-role.js';

/**
 * Calls data functions to validate the user's information for logging in
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @param {string} email email to log in with
 * @param {string} password password to log in with
 * @return {Object} { error if exists, user data if no error }
 */

export const logInUser = (usersData) => async (email, password) => {
  const user = await usersData.getSingleUserByEmail(email);

  if (!(user)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  } else if (!(await bcrypt.compare(password, user.password))) {
    return {
      error: errors.INVALID_SIGNIN,
      data: null,
    };
  }

  return {
    error: null,
    data: user,
  };
};

/**
 * Calls data functions to validate the user's information to show all Users
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */

export const getUsers = (usersData) => async () => {
  const users = await usersData.getAllUsers();
  if (!(users)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: users,
  };
};

/**
 * Calls data functions to validate the user's information to show all Users by email
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @param {string} email email to log in with
 * @return {Object} { error if exists, user data if no error }
 */

export const getUsersByEmail = (usersData) => async (email) => {
  const user = await usersData.getUsersByEmail(email);

  if (user === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: user,
  };
};

/**
 * Calls data functions to validate the user's information to show all Users by name
 *
 * @param {Object} usersData collection of functions that send queries to the database
 * @param {string} name name to log in with
 * @return {Object} { error if exists, user data if no error }
 */

export const getUsersByName = (usersData) => async (name) => {
  const user = await usersData.getUsersByFullName(name);

  if (user === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: user,
  };
};

/**
 * Calls data functions to validate the user's information to show all Users by name and email
 * @param {Object} searchData search data to search for specific users
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */

export const searchUser = (usersData) => async (searchData) => {
  const users = await usersData.searchUser(searchData);

  if (users === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: users,
  };
};

/**
 * Calls data functions to validate the user's information to show all incoming parcels by user id
 * @param {number} userId id of user
 * @param {Object} user user data
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */

export const getIncomingParcels = (usersData) => async (userId, user) => {
  if (user.role === userRole.CUSTOMER && userId !== user.id) {
    return {
      error: errors.FORBIDDEN,
      data: null,
    };
  }

  const incomingParcels = await usersData.getIncomingParcels(userId);

  if (!incomingParcels) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: incomingParcels,
  };
};

/**
 * Calls data functions to validate the user's information to create a new user
 * @param {Object} user user data
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */

export const registerUser = (usersData) => async (user) => {
  if (await usersData.userExists(user.email)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }
  const hashPassword = await bcrypt.hash(user.password, 10);
  const createdUser = await usersData.createUser(user, hashPassword);
  if (createdUser) {
    return {
      error: null,
      data: createdUser,
    };
  }
  return {
    error: errors.INVALID_REQUEST,
    data: null,
  };
};

/**
 * Calls data functions to validate the user's information to update a user
 * @param {Object} body user data
 * @param {number} userId id of user
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */

export const updateUser = (usersData) => async (userId, body) => {
  const user = await usersData.getUserById(userId);

  if (user) {
    const updatedUser = await usersData.updateUser(userId, body);
    if (updatedUser) {
      return {
        error: null,
        updatedUser: updatedUser,
      };
    }
  }

  return {
    error: errors.NOT_FOUND,
    data: null,
  };
};

/**
 * Calls data functions to validate the user's information to delete a user
 * @param {number} userId id of user
 * @param {Object} usersData collection of functions that send queries to the database
 * @return {Object} { error if exists, user data if no error }
 */

export const deleteUser = (usersData) => async (userId) => {
  const user = await usersData.getUserById(userId);

  if (user) {
    if (user.role === userRole.EMPLOYEE) {
      return {
        data: null,
        error: errors.FORBIDDEN,
      };
    }

    const deletedUser = await usersData.deleteUser(userId);
    if (deleteUser) {
      return {
        error: null,
        data: deletedUser,
      };
    }
  }

  return {
    error: errors.NOT_FOUND,
    data: null,
  };
};

export default {
  logInUser,
  getUsers,
  getUsersByEmail,
  getUsersByName,
  searchUser,
  getIncomingParcels,
  registerUser,
  updateUser,
  deleteUser,
};
