import errors from './errors.js';

/**
 * Calls data functions to validate the shipment's information to show all shipments
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @return {Object} { error if exists, shipment data if no error }
 */

export const getAllShipments = (shipmentData) => async () => {
  const allShipments = await shipmentData.getAllShipments();
  if (!allShipments) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: allShipments,
  };
};

/**
 * Calls data functions to validate the shipment's information to show all shipments by customer id
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @param {number} customerId user id
 * @return {Object} { error if exists, shipment data if no error }
 */

export const getAllShipmentsByCustomerId = (shipmentData) => async (customerId) => {
  const shipmentsByCustomerId = await shipmentData.getAllshipmentsByCustomerId(customerId);
  if (!shipmentsByCustomerId) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: shipmentsByCustomerId,
  };
};

/**
 * Calls data functions to validate the shipment's information to show all shipments by status id
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @param {number} statusId status id
 * @return {Object} { error if exists, shipment data if no error }
 */

 export const getAllShipmentsByStatusId = (shipmentData) => async () => {
  const shipmentsByStatusId = await shipmentData.getShipmentsByStatusId();
  if (!shipmentsByStatusId) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: shipmentsByStatusId,
  };
};

/**
 * Calls data functions to validate the shipment's information to show all shipments ny warehouse id
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @param {number} warehouseId user id
 * @return {Object} { error if exists, shipment data if no error }
 */

export const getAllShipmentsByWarehouseId = (shipmentData) => async (warehouseId) => {
  const shipmentsByWarehouseId = await shipmentData.getAllshipmentsByWarehouseId(warehouseId);
  if (!shipmentsByWarehouseId) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: shipmentsByWarehouseId,
  };
};

/**
 * Calls data functions to validate the shipment's information to show a single shipment by id
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @param {number} shipmentId shipment id
 * @return {Object} { error if exists, shipment data if no error }
 */
export const getShipmentById = (shipmentData) => async (shipmentId) => {
  const shipmentsByWarehouseId = await shipmentData.getShipmentById(shipmentId);
  if (!shipmentsByWarehouseId) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: shipmentsByWarehouseId,
  };
};

/**
 * Sends SQL request to database to create a shipment
 * @param {Date} arrivalDate date of arrival
 * @param {Date} departureDate date of departure
 * @param {number} destinationWarehouseId id of destination warehouse
 * @param {number} originWarehouseId id of origin warehouse
 * @param {number} status status of shipment
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @return {Object} { error if exists, shipment data if no error }
 */


export const createShipment = (shipmentData) => async (originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate) => {
  const createdShipment = await shipmentData.createShipment(originWarehouseId, destinationWarehouseId, status, arrivalDate, departureDate);

  if (!createdShipment) {
    return {
      error: errors.OPERATION_FAILED,
      data: null,
    };
  }
  return {
    error: null,
    data: createdShipment,
  };
};

/**
 * Sends SQL request to database to update a shipment by id
 * @param {object} body shipment update data
 * @param {number} shipmentId id of shipment
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @return {Object} { error if exists, shipment data if no error }
 */

export const updateShipment = (shipmentData) => async (shipmentId, body) => {
  const updatedShipment = await shipmentData.updateShipment(shipmentId, body);

  if (!updatedShipment) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: updatedShipment,
  };
};

/**
 * Sends SQL request to database to delete a shipment by id
 * @param {number} shipmentId id of shipment
 * @param {Object} shipmentData collection of functions that send queries to the database
 * @return {Object} { error if exists, shipment data if no error }
 */

export const deleteShipment = (shipmentData) => async (shipmentId) => {
  const deletedShipment = await shipmentData.deleteShipment(shipmentId);

  if (!deletedShipment) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: deletedShipment,
  };
};


export default {
  getAllShipments,
  getAllShipmentsByCustomerId,
  getAllShipmentsByWarehouseId,
  getAllShipmentsByStatusId,
  createShipment,
  updateShipment,
  deleteShipment,
};
