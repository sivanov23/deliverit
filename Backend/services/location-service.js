import errors from '../services/errors.js';

/**
 * Calls data functions to validate the country's information to show all countries
 * @param {Object} locationData collection of functions that send queries to the database
 * @return {Object} { error if exists, location data if no error }
 */

export const getAllCountries = (locationData) => async () => {
  const countries = await locationData.getAllCountries();

  if (countries === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: countries,
  };
};

/**
 * Calls data functions to validate the city's information to show all cities
 * @param {Object} locationData collection of functions that send queries to the database
 * @return {Object} { error if exists, location data if no error }
 */

export const getAllCities = (locationData) => async () => {
  const countries = await locationData.getAllCities();

  if (countries === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: countries,
  };
};

/**
 * Calls data functions to validate the city's information to show all cities by country id
 * @param {number} countryId country id
 * @param {Object} locationData collection of functions that send queries to the database
 * @return {Object} { error if exists, location data if no error }
 */

export const getCitiesByCountryId = (locationData) => async (countryId) => {
  const countries = await locationData.getCitiesByCountryId(countryId);

  if (countries === null) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }
  return {
    error: null,
    data: countries,
  };
};


export default { getAllCountries, getAllCities, getCitiesByCountryId };
