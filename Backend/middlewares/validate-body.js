export default (schema) => (req, res, next) => {
  const body = req.body;
  const validations = Object.keys(schema);

  const fails = validations
    .map((key) => schema[key](body[key]))
    .filter((error) => error !== null);

  if (fails.length > 0) {
   return res.status(400).send({
      data: null,
      error: fails[0],
    });
  }
  return next();
};
