import passport from 'passport';

// tells to use the jwt strategy
const authMiddleware = passport.authenticate('jwt', { session: false });

export default authMiddleware;
