import userRole from '../common/user-role.js';

export default (req, res, next) => {
  const role = req.user.role;

  if (role === userRole.CUSTOMER) {
    res.status(403).send({
      data: null,
      error: 'User does not have the right security level to complete this operation',
    });
  } else {
    next();
  }
};
