# DeliverIT

How to run the project:

1. Clone repo from git: https://gitlab.com/sivanov23/deliverit

2. You will find the following folders: 
   -Backend
   -Frontend
   -Wireframes

  Open both Backend and Frontend and type npm install in terminal

3.On the Backend side, use the Deliver_it.sql file to create the schema for the database. You can do this for eample in mySQL Workbench by File-> OpenSqlScript

4.There is seeded data, which can be immediately used, after running npm run seed in the terminal

5.In order to be able to run the server and connect to the database, the following run configurations are required. Put those in a .env file in the main directory of the Backend folder(on the same level where package.json is located). The password is the same that you use to connect to the local instance of your database, but is not shown for security reasons.

Run configurations: 

SERVER_PORT = 5000
HOST = localhost
DB_PORT = 3306
DB_USER = root
DB_PASSWORD = **your database password goes here**
DATABASE = deliver_it
SECRET_KEY = Shm3ntikapel!

6.Open two separate instances of the terminal.

In one of them, navigate to the Backend folder and input npm run start in the terminal to start the server.

In the other one, navigate to the Frontend folder and input npm run start in the terminal to start the client.

The client should automatically open a window in your default browser and you will now have access to it.

If you want to use a user with Employee privileges, you can log in with

  email: sample.employee@abv.bg 
  password: Adminpass

  email: sample.customer@gmail.com
  password: Adminpass

7.There is included swagger API documentation of our Backend endpoints called Swagger Documentation.html You can open it by double clicking it in your browser.

